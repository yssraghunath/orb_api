package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.CenterAddress;
import com.bro.orb.domain.ExaminationCenter;

@Repository
public interface ExamCenterRepository extends JpaRepository<ExaminationCenter, Long> {

	@Query("Select c from CenterAddress c, ExaminationCenter e where e.address.centerAddress = ?1")
	List<CenterAddress> findByName(String name);
	
}
