package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.TechnicalQualification;

@Repository
public interface TechnicalQualificationRepository extends JpaRepository<TechnicalQualification, Long>{

}
