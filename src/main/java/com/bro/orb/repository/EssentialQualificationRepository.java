package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.EssentialQualification;

@Repository
public interface EssentialQualificationRepository extends JpaRepository<EssentialQualification, Long> {

}
