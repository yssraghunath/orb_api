package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

	@Query("Select c from Course c where c.courseType = ?1")
	List<Course> findByOne(String type);
	
	@Query("Select distinct(c.courseType) from Course c")
	List<Course> findAll();
}
