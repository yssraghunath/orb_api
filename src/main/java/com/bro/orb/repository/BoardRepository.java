package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.Board;

@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {

}
