package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.RegistrationViewBean;

@Repository
public interface RegistrationViewRepository extends JpaRepository<RegistrationViewBean, Long> {

}
