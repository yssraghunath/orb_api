package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.JobDetails;

@Repository
public interface JobDetailsRepository extends JpaRepository<JobDetails, Long> {

	List<JobDetails> findAll();

	@Modifying
	@Query("update JobDetails job set job.postDetailsDocument = ?1 where job.id = ?2")
	void update(String jobDocument, long jobid);

}
