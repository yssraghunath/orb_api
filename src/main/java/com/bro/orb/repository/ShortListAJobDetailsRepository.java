package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bro.orb.domain.ShortListAJobDetails;

public interface ShortListAJobDetailsRepository extends JpaRepository<ShortListAJobDetails, Long> {

}
