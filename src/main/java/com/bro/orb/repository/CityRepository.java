package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

	@Query("Select c from City c where c.state.id = ?1")
	List<City> findById(int id);

	@Query("Select c from City c where c.state.name = ?1")
	List<City> findByName(String name);
	

}
