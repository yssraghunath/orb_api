package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.PersonalDetails;

@Repository
public interface PersonalRepository extends JpaRepository<PersonalDetails, Long> {

	@Query("Select c from PersonalDetails c where c.registrationId = ?1")
	List<PersonalDetails> findForRegistrationId(String registrationId);

	@Query("Select c from PersonalDetails c where c.category = ?1")
	List<PersonalDetails> findForCategory(String category);

}
