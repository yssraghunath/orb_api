package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.Zone;

@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {


}
