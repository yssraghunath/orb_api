/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bro.orb.web.rest.vm;
