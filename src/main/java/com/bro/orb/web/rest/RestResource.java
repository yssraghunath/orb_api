package com.bro.orb.web.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import liquibase.util.StringUtils;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bro.orb.domain.Board;
import com.bro.orb.domain.CandidateListFilter;
import com.bro.orb.domain.Category;
import com.bro.orb.domain.City;
import com.bro.orb.domain.Course;
import com.bro.orb.domain.ExaminationCenter;
import com.bro.orb.domain.JobAmountDetails;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.PersonalDetails;
import com.bro.orb.domain.RegistrationBean;
import com.bro.orb.domain.RegistrationViewBean;
import com.bro.orb.domain.State;
import com.bro.orb.domain.University;
import com.bro.orb.domain.User;
import com.bro.orb.domain.Years;
import com.bro.orb.repository.JobAmountRepository;
import com.bro.orb.service.IGenericService;
import com.bro.orb.service.MailService;
import com.bro.orb.service.RegisterService;
import com.bro.orb.service.RestCommonService;

import ch.qos.logback.core.net.SyslogOutputStream;

@RestController
@RequestMapping("/api/register")
public class RestResource {

	private final Logger log = LoggerFactory.getLogger(RestResource.class);

	@Autowired
	private IGenericService<PersonalDetails> personalService;

	@Autowired
	private IGenericService<JobAmountDetails> jobAmountDetails;

	@Autowired
	private IGenericService<RegistrationViewBean> rvbService;

	@Autowired
	private IGenericService<JobDetails> jobDeatails;

	@Autowired
	private IGenericService<Category> category;

	@Autowired
	private IGenericService<Years> years;

	@Autowired
	private RegisterService registerService;

	@Autowired
	private RestCommonService restService;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private IGenericService<Course> course;

	@RequestMapping(path = "/candidate/register")
	public ResponseEntity<?> registerCandidate(@ModelAttribute RegistrationBean personal,
			@RequestParam(value = "categoryCertificateDoc", required = false) MultipartFile categoryDoc,
			@RequestParam(value = "nccBCertificateDoc", required = false) MultipartFile nccBCertificateDoc,
			@RequestParam(value = "nccCCertificateDoc", required = false) MultipartFile nccCCertificateDoc,
			@RequestParam(value = "sportsManDoc", required = false) MultipartFile sportsManDoc,
			@RequestParam(value = "aadhaarCertificateDoc", required = false) MultipartFile aadhaarCertificateDoc,
			@RequestParam(value = "highCertificateDoc", required = false) MultipartFile highCertificateDoc,
			@RequestParam(value = "interCertificateDoc", required = false) MultipartFile interCertificateDoc,
			@RequestParam(value = "additionalCertificateDoc", required = false) MultipartFile additionalCertificateDoc,
			@RequestParam(value = "armedCertificateDoc", required = false) MultipartFile armedCertificateDoc,
			@RequestParam(value = "firstaidCertificateDoc", required = false) MultipartFile firstaidCertificateDoc,
			
			@RequestParam(value = "ugCertificateDoc", required = false) MultipartFile ugCertificateDoc,
			@RequestParam(value = "pgCertificateDoc", required = false) MultipartFile pgCertificateDoc,
			@RequestParam(value = "diplomaCertificateDoc", required = false) MultipartFile diplomaCertificateDoc,
			@RequestParam(value = "degreeCertificateDoc", required = false) MultipartFile degreeCertificateDoc,
			@RequestParam(value = "itiCertificateDoc", required = false) MultipartFile itiCertificateDoc,
			@RequestParam(value = "experienceCertificateDoc", required = false) MultipartFile experienceCertificateDoc,
			@RequestParam(value = "drivingLicenseDoc", required = false) MultipartFile drivingLicenseDoc,
			@RequestParam(value = "drivingExperienceDoc", required = false) MultipartFile drivingExperienceDoc,
			@RequestParam(value = "browsePhotoDoc", required = false) MultipartFile browsePhotoDoc,
			@RequestParam(value = "centralCertificateDoc", required = false) MultipartFile centralCertificateDoc,
			@RequestParam(value = "browseSignatureDoc", required = false) MultipartFile browseSignatureDoc) {
		log.debug("Candidate Registration for: " + personal.getFirstName() + "," + personal.getEmailAddress());

		registerService.registerCandidate(personal, categoryDoc, nccBCertificateDoc, nccCCertificateDoc, sportsManDoc,
				aadhaarCertificateDoc, highCertificateDoc, interCertificateDoc, additionalCertificateDoc,
				ugCertificateDoc, pgCertificateDoc, diplomaCertificateDoc, degreeCertificateDoc,itiCertificateDoc, browsePhotoDoc,
				browseSignatureDoc, drivingExperienceDoc, drivingLicenseDoc, experienceCertificateDoc, centralCertificateDoc,armedCertificateDoc,firstaidCertificateDoc);
		mailService.sendRegistrationEmail(personal);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/candidate/form/{jobid}")
	public ResponseEntity<JobDetails> candidateForm(@PathVariable("jobid") int jobid) {
		JobDetails jobDetails = jobDeatails.findOneByCondition(new JobDetails(), "id = " + jobid + " and isActive = 1");
		if (jobDetails.isActive()) {
			return new ResponseEntity<JobDetails>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<JobDetails>(jobDetails, HttpStatus.OK);
	}

	@RequestMapping(path = "/form/fields/{jobid}")
	public ResponseEntity<RegistrationViewBean> registrationView(@PathVariable("jobid") int jobid) {
		RegistrationViewBean formview = rvbService.findOneByCondition(new RegistrationViewBean(), "job_id = " + jobid);
		if (formview == null) {
			return new ResponseEntity<RegistrationViewBean>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<RegistrationViewBean>(formview, HttpStatus.OK);
	}

	@SuppressWarnings("unused")
	@RequestMapping(path = "/form/amount/{amountid}")
	public ResponseEntity<List<JobAmountDetails>> jobAnountDetail(@PathVariable("amountid") int amountid) {
		List<JobAmountDetails> jobAmountview = jobAmountDetails.findAllByCondition(new JobAmountDetails(),
				"where view_id = " + amountid);

		for (JobAmountDetails details : jobAmountview) {
			Date expired = details.getJobDetails().getExpiryDate();
			int minAge = details.getMinAge();

			String minDate = registerService.getMaxDate(minAge, expired);
			details.setMinAgeDate(minDate);
			int maxAge = details.getMaxAge();

			String maxDate = registerService.getMaxDate(maxAge, expired);
			details.setMaxAgeDate(maxDate);

		}
		if (jobAmountview == null) {
			return new ResponseEntity<List<JobAmountDetails>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<JobAmountDetails>>(jobAmountview, HttpStatus.OK);

	}

	/**
	 * @param personal
	 * @throws IOException
	 */
	@RequestMapping(path = "/emailCheck", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public Optional<User> checkForEmail(@RequestParam(value = "email") String email) {
		log.debug("email check for: " + email);
		return restService.checkForEmail(email);
	}

	@RequestMapping(path = "/active/jobs", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<JobDetails> findActiveJobs() {
		return jobDeatails.findAllByCondition(new JobDetails(), " where '"+new DateTime()+"' between activeDate and expiryDate ORDER BY id DESC");
	}

	@RequestMapping(path = "/state", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<State> findState() {
		return restService.findState();
	}

	@RequestMapping(path = "/city/{name}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<City> findCity(@PathVariable(value = "name") String name) {
		log.debug("Geeting city for: " + name);
		return restService.findByName(name);
	}

@RequestMapping(path = "/courses", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<Course> findAllCourse() {
		return restService.findCourseType();
	
	}

	@RequestMapping(path = "/boards", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<Board> findBoard() {
		return restService.findBoard();
	}

	@RequestMapping(path = "/courses/type", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<Course> findCourse(@RequestParam(value = "type") String type) {
		return restService.findCourse(type);
	}

	@RequestMapping(path = "/university", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<University> findUniversity() {
		return restService.findUniversity();
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(path = "/upload", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> fileUpload(@RequestParam(value = "file") MultipartFile file) {

		try {
			Date date = new Date();
			long lon = date.getTime();
			String folder_name = "src/main/resources/upload/" + lon + "/";
			File files = new File(folder_name);
			files.mkdirs();
			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			Path path = Paths.get(folder_name + file.getOriginalFilename());
			Files.write(path, bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new ResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(path = "/personal", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<PersonalDetails>> personalDetails() {

		List<PersonalDetails> list = personalService.findAll(new PersonalDetails());
		if (list == null || list.isEmpty()) {
			return new ResponseEntity<List<PersonalDetails>>(list, HttpStatus.NO_CONTENT);
		}
		Collections.reverse(list);
		return new ResponseEntity<List<PersonalDetails>>(list, HttpStatus.OK);
	}

	@RequestMapping(path = "/personal/filter", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<PersonalDetails>> filterCandidateList(@RequestBody CandidateListFilter filter) {

		
		List<PersonalDetails> pdList = personalService.findAllByCondition(new PersonalDetails(),
				getFilterQuery(filter));
		if (pdList.size() == 0) {
			return new ResponseEntity<List<PersonalDetails>>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<List<PersonalDetails>>(pdList, HttpStatus.OK);
	}

	@RequestMapping(path = "/personal/{registrationid}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<PersonalDetails> getPersonal(@PathVariable("registrationid") String registrationid) {
		PersonalDetails personal = personalService.findOneByCondition(new PersonalDetails(),
				"registrationId = " + registrationid);
		return new ResponseEntity<PersonalDetails>(personal, HttpStatus.OK);
	}

	@RequestMapping(path = "/category", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<Category> findCategory() {
		return category.findAll(new Category());
	}

	@RequestMapping(path = "/year", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<Years> findYear() {
		return years.findAll(new Years());
	}
	private String getFilterQuery(CandidateListFilter clf) {
		String query = "";
		
		if (StringUtils.isNotEmpty(clf.getRegistrationId())) {
			query = query + " where registrationId = '" + clf.getRegistrationId() + "'";
		}
		if (!query.isEmpty() && clf.isNccb()) {
			query = query + " and nccB = '" + clf.isNccb() + "'";
		} else if (clf.isNccb()) {
			query = query + " where nccB  = '" + clf.isNccb() + "'";
		}
		if (!query.isEmpty() && clf.isNccc()) {
			query = query + " and nccC  = '" + clf.isNccc() + "'";
		} else if (clf.isNccc()) {
			query = query + " where nccC  = '" + clf.isNccc() + "'";
		}
		if (!query.isEmpty() && clf.isSportsman()) {
			query = query + " and sportsman  = '" + clf.isSportsman() + "'";
		} else if (clf.isSportsman()) {
			query = query + " where sportsman  = '" + clf.isSportsman() + "'";
		}
		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getNonindian())) {
			query = query + " and nationality  = '" + clf.getNonindian() + "'";
		} else if (StringUtils.isNotEmpty(clf.getNonindian())) {
			query = query + " where nationality  = '" + clf.getNonindian() + "'";
		}
		if (!query.isEmpty() && clf.isSondaughterGref()) {
			query = query + " and sonDaughterGref  = '" + clf.isSondaughterGref() + "'";
		} else if (clf.isSondaughterGref()) {
			query = query + " where sonDaughterGref  = '" + clf.isSondaughterGref() + "'";
		}
		if (!query.isEmpty() && clf.isBrosister()) {
			query = query + " and brotherSisterGref  = '" + clf.isBrosister() + "'";
		} else if (clf.isBrosister()) {
			query = query + " where brotherSisterGref  = '" + clf.isBrosister() + "'";
		}
		if (!query.isEmpty() && clf.isDisable()) {
			query = query + " and disablity  = '" + clf.isDisable() + "'";
		} else if (clf.isDisable()) {
			query = query + " where disablity  = '" + clf.isDisable() + "'";
		}
		if (!query.isEmpty() && clf.isCentrakgov()) {
			query = query + " and centralGovt  = '" + clf.isCentrakgov() + "'";
		} else if (clf.isCentrakgov()) {
			query = query + " where centralGovt  = '" + clf.isCentrakgov() + "'";
		}
		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPostName())) {
			query = query + " and obj.jobDetails.postName  = '" + clf.getPostName() + "'";
		} else if (StringUtils.isNotEmpty(clf.getPostName())) {
			query = query + " where obj.jobDetails.postName = '" + clf.getPostName() + "'";
			System.out.println(query);
		}

		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getCandidateName())) {
			query = query + " and firstName  = '" + clf.getCandidateName() + "'";
		} else if (StringUtils.isNotEmpty(clf.getCandidateName())) {
			query = query + " where firstName = '" + clf.getCandidateName() + "'";
			System.out.println(query);
		}

		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getCategory())) {
			query = query + " and category  = '" + clf.getCategory() + "'";
		} else if (StringUtils.isNotEmpty(clf.getCategory())) {
			query = query + " where category = '" + clf.getCategory() + "'";
			System.out.println(query);
		}

		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getQualification())) {
			// query = query + " and category = '" + clf.getQualification()+ "'";

			
		} else if (StringUtils.isNotEmpty(clf.getQualification())) {
			// query = query + " where category = '" + clf.getQualification() + "'";
			
			if (clf.getQualification().equalsIgnoreCase("highschool")) {
				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.essential.highPercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.essential.highPercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("Intermediate")) {

				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.essential.interPercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.essential.interPercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("Graduation")) {

				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.graduation.ugPercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.graduation.ugPercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("PostGraduation")) {

				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.graduation.pgPercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.graduation.pgPercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("Diploma")) {
				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.technical.diplomaPercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.technical.diplomaPercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("Degree")) {

				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " obj.technical.degreePercentage  >= '" + clf.getPercentage() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.technical.degreePercentage >= '" + clf.getPercentage() + "'";
					System.out.println(query);
				}
			}
		}
		if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getQualification())) {
			// query = query + " and category = '" + clf.getQualification()+ "'";

			
			
		} else if (StringUtils.isNotEmpty(clf.getQualification())) {
			// query = query + " where category = '" + clf.getQualification() + "'";
			if (clf.getQualification().equalsIgnoreCase("highschool")) {
				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getGrade())) {
					query = query + " obj.essential.highgrade  = '" + clf.getGrade() + "'";
				} else if (StringUtils.isNotEmpty(clf.getGrade())) {
					query = query + " where obj.essential.highgrade = '" + clf.getGrade() + "'";
					System.out.println(query);
				}
			} else if (clf.getQualification().equalsIgnoreCase("Intermediate")) {

				if (!query.isEmpty() && StringUtils.isNotEmpty(clf.getGrade())) {
					query = query + " obj.essential.intergrade  = '" + clf.getGrade() + "'";
				} else if (StringUtils.isNotEmpty(clf.getPercentage())) {
					query = query + " where obj.essential.intergrade = '" + clf.getGrade() + "'";
					System.out.println(query);
				}
			}
		}

		if (!query.isEmpty() && clf.isAge()) {
			query = query + " ORDER BY dateOfBirth";
			// and dateOfBirth = '" + clf.isAge() + "'
			// FROM Employee E WHERE E.id > 10 ORDER BY E.salary DESC
		} else if (clf.isAge()) {
			query = query + "  ORDER BY dateOfBirth";
			// where dateOfBirth = '" + clf.isAge() + "'
			// FROM Employee E WHERE E.id > 10 ORDER BY E.salary DESC
		}
		return query;
	}
}
