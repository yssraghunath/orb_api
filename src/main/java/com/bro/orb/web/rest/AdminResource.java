package com.bro.orb.web.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bro.orb.domain.AdminRegister;
import com.bro.orb.domain.CenterAddress;
import com.bro.orb.domain.ExaminationCenter;
import com.bro.orb.domain.ExaminationCenterAllocation;
import com.bro.orb.domain.HistorySmsEmail;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.JobFormFields;
import com.bro.orb.domain.JobsDescription;
import com.bro.orb.domain.JobsFormDescription;
import com.bro.orb.domain.ShortListAPersonalDetails;
import com.bro.orb.repository.ExamCenterRepository;
import com.bro.orb.service.AdminService;
import com.bro.orb.service.IGenericService;

@RestController
@RequestMapping("/api/admin")
public class AdminResource {

	private final Logger log = LoggerFactory.getLogger(RestResource.class);

	@Autowired
	private AdminService adminService;

	@Autowired
	private IGenericService<JobsDescription> jobsDescription;

	@Autowired
	private IGenericService<ShortListAPersonalDetails> shortlistPersonalService;

	@Autowired
	private IGenericService<ExaminationCenter> examinationCenterService;

	@Autowired
	private IGenericService<ExaminationCenterAllocation> centerAllocationService;

	@Autowired
	private IGenericService<HistorySmsEmail> historySmsEmailService;

	@Autowired
	private IGenericService<JobDetails> jobDetailsService;

	@Autowired
	private ExamCenterRepository centerRepository;

	@PostMapping(value = "/save/job", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public List<JobsDescription> jobSave(@RequestBody List<JobsFormDescription> jobsFormDesc) {
		log.info("saving jobs for" + jobsFormDesc);
		return adminService.saveJobs(jobsFormDesc);
	}

	@PostMapping(value = "/register", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> adminRegister(@RequestBody AdminRegister register) {
		log.info("Registration for : " + register.getFirstName());
		adminService.adminRegister(register);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PostMapping(value = "/create/center", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> createCenter(@RequestBody ExaminationCenter examinationCenter) {
		log.info("Create center for : " + examinationCenter.getCenterName());
		adminService.saveCenter(examinationCenter);
		return new ResponseEntity<>(centerRepository.findAll(), HttpStatus.CREATED);
	}

	@PostMapping(value = "/allocate/center", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> allocateCenter(@RequestBody ExaminationCenterAllocation centerAllocation) {
		log.info("Center Allocation for : " + centerAllocation.getCenterName());
		centerAllocationService.save(centerAllocation);
		return new ResponseEntity<>(centerAllocationService.findAll(new ExaminationCenterAllocation()), HttpStatus.OK);
	}

	@PostMapping(value = "/save/job/fields", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> jobFieldsSave(@RequestBody JobFormFields jobFormFields) {
		log.info("saving job fields for" + jobFormFields);
		adminService.saveFields(jobFormFields);
		return new ResponseEntity<>(jobDetailsService.findOneByCondition(new JobDetails(), " order by id ", 0),
				HttpStatus.OK);
	}

	@PostMapping(value = "/candidate/history", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> saveHistory(@RequestBody HistorySmsEmail history) {
		log.info("Admit card history for : " + history.getFirstName());
		historySmsEmailService.save(history);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/upload/job/document")
	public ResponseEntity<?> uploadJobDocument(@RequestParam(value = "jobId") long jobid,
			@RequestParam(value = "postDetailsDocument", required = false) MultipartFile postDetailsDocument) {
		log.info("Uploading Job Document for: " + jobid);
		adminService.uploadJobDocument(postDetailsDocument, jobid);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(path = "/total/jobs", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<List<JobsDescription>> findAllJobs() {
		List<JobsDescription> allJob = new ArrayList<JobsDescription>();
		allJob = jobsDescription.findAll(new JobsDescription());
		Collections.reverse(allJob);
		return new ResponseEntity<>(allJob, HttpStatus.OK);
	}

	@RequestMapping(path = "/remove", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteJob(@RequestBody JobsDescription jobsDesc) {
		jobsDescription.delete(jobsDesc);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(path = "/update/job", produces = { MediaType.APPLICATION_JSON_VALUE })
	public JobsDescription updateJob(@RequestBody JobsDescription jobDesc) {
		return jobsDescription.update(jobDesc);
	}

	@RequestMapping(path = "/all/centers", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<List<ExaminationCenter>> findAllCenters() {
		List<ExaminationCenter> allCenters = new ArrayList<ExaminationCenter>();
		allCenters = examinationCenterService.findAll(new ExaminationCenter());
		Collections.reverse(allCenters);
		return new ResponseEntity<>(allCenters, HttpStatus.OK);
	}

	@RequestMapping(path = "/all/allocation/center", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<List<ExaminationCenterAllocation>> findAllAllocationsCenters() {
		List<ExaminationCenterAllocation> allAllocationCenters = new ArrayList<ExaminationCenterAllocation>();
		allAllocationCenters = centerAllocationService.findAll(new ExaminationCenterAllocation());
		Collections.reverse(allAllocationCenters);
		return new ResponseEntity<>(allAllocationCenters, HttpStatus.OK);
	}

	@RequestMapping(path = "/delete/center", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteJob(@RequestBody ExaminationCenter examinationCenter) {
		examinationCenterService.delete(examinationCenter);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(path = "/update/center", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ExaminationCenter updateJob(@RequestBody ExaminationCenter examinationCenter) {
		return examinationCenterService.update(examinationCenter);
	}

	
	@PostMapping(value = "/shortlist/candidate", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> shortListCandidate(@RequestBody List<ShortListAPersonalDetails> personal) {
		log.info("Shortlisting for : " + personal.size());
		adminService.saveShortListCandidate(personal);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(path = "/shortlisted/all/candidate", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<List<ShortListAPersonalDetails>> findAllCandidates() {
		List<ShortListAPersonalDetails> candidates = new ArrayList<ShortListAPersonalDetails>();
		candidates = shortlistPersonalService.findAll(new ShortListAPersonalDetails());
		return new ResponseEntity<>(candidates, HttpStatus.OK);
	}
	
	
}
