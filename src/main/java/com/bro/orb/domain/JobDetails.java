package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bro_job_details")
public class JobDetails extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "post_name")
	private String postName;

	@Column(name = "post_profile")
	private String postProfile;

	@Column(name = "post_details_document")
	private String postDetailsDocument;

	@Column(name = "active_date")
	private Date activeDate;

	@Column(name = "expiry_date")
	private Date expiryDate;

	@Column(name = "fee_start_date")
	private Date feeSubmitDate;

	@Column(name = "fee_last_date")
	private Date feeSubmitLastDate;

	@Column(name = "active")
	private boolean active;

	public JobDetails() {
		super();
	}

	public JobDetails(long id, String postName, String postProfile,
			String postDetailsDocument, Date activeDate, Date expiryDate,
			Date feeSubmitDate, Date feeSubmitLastDate, boolean active) {
		super();
		this.id = id;
		this.postName = postName;
		this.postProfile = postProfile;
		this.postDetailsDocument = postDetailsDocument;
		this.activeDate = activeDate;
		this.expiryDate = expiryDate;
		this.feeSubmitDate = feeSubmitDate;
		this.feeSubmitLastDate = feeSubmitLastDate;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public String getPostProfile() {
		return postProfile;
	}

	public void setPostProfile(String postProfile) {
		this.postProfile = postProfile;
	}

	public String getPostDetailsDocument() {
		return postDetailsDocument;
	}

	public void setPostDetailsDocument(String postDetailsDocument) {
		this.postDetailsDocument = postDetailsDocument;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getFeeSubmitDate() {
		return feeSubmitDate;
	}

	public void setFeeSubmitDate(Date feeSubmitDate) {
		this.feeSubmitDate = feeSubmitDate;
	}

	public Date getFeeSubmitLastDate() {
		return feeSubmitLastDate;
	}

	public void setFeeSubmitLastDate(Date feeSubmitLastDate) {
		this.feeSubmitLastDate = feeSubmitLastDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "JobDetails [id=" + id + ", postName=" + postName
				+ ", postProfile=" + postProfile + ", postDetailsDocument="
				+ postDetailsDocument + ", activeDate=" + activeDate
				+ ", expiryDate=" + expiryDate + ", feeSubmitDate="
				+ feeSubmitDate + ", feeSubmitLastDate=" + feeSubmitLastDate
				+ ", active=" + active + "]";
	}
}
