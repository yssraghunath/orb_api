package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name="bro_shortlist1_experience")
@Entity
public class ShortListAOrganisationExperience extends AbstractAuditingEntity implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	
	@Column(name = "organization_name")
	private String companyName;
	
	
	@Column(name = "employment_period_from")
	private Date employmentFrom;
	
	
	@Column(name = "employment_period_to")
	private Date employmentTo;
	
	
	@Column(name = "cin_number")
	private String cinNumber;
	
	@Column(name = "regtin_number")
	private String regtinNumber;
	
	
	@Column(name = "work_nature")
	private String workNature;
	
	
	@Column(name = "monthly_salary")
	private long monthlySalary;
	
	
	@Column(name = "temporary_permanent")
	private String temporaryPermanent;
	
	
	@Column(name = "experience_certificate")
	private String experienceCertificate;
	

	public ShortListAOrganisationExperience() {
		super();
	}


	public ShortListAOrganisationExperience(long id, String companyName, Date employmentFrom, Date employmentTo, String cinNumber,
			String regtinNumber, String workNature, long monthlySalary, String temporaryPermanent,
			String experienceCertificate) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.employmentFrom = employmentFrom;
		this.employmentTo = employmentTo;
		this.cinNumber = cinNumber;
		this.regtinNumber = regtinNumber;
		this.workNature = workNature;
		this.monthlySalary = monthlySalary;
		this.temporaryPermanent = temporaryPermanent;
		this.experienceCertificate = experienceCertificate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Date getEmploymentFrom() {
		return employmentFrom;
	}


	public void setEmploymentFrom(Date employmentFrom) {
		this.employmentFrom = employmentFrom;
	}


	public Date getEmploymentTo() {
		return employmentTo;
	}


	public void setEmploymentTo(Date employmentTo) {
		this.employmentTo = employmentTo;
	}


	public String getCinNumber() {
		return cinNumber;
	}


	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}


	public String getRegtinNumber() {
		return regtinNumber;
	}


	public void setRegtinNumber(String regtinNumber) {
		this.regtinNumber = regtinNumber;
	}


	public String getWorkNature() {
		return workNature;
	}


	public void setWorkNature(String workNature) {
		this.workNature = workNature;
	}


	public long getMonthlySalary() {
		return monthlySalary;
	}


	public void setMonthlySalary(long monthlySalary) {
		this.monthlySalary = monthlySalary;
	}


	public String getTemporaryPermanent() {
		return temporaryPermanent;
	}


	public void setTemporaryPermanent(String temporaryPermanent) {
		this.temporaryPermanent = temporaryPermanent;
	}


	public String getExperienceCertificate() {
		return experienceCertificate;
	}


	public void setExperienceCertificate(String experienceCertificate) {
		this.experienceCertificate = experienceCertificate;
	}


	@Override
	public String toString() {
		return "OrganisationExperience [id=" + id + ", companyName=" + companyName + ", employmentFrom="
				+ employmentFrom + ", employmentTo=" + employmentTo + ", cinNumber=" + cinNumber + ", regtinNumber="
				+ regtinNumber + ", workNature=" + workNature + ", monthlySalary=" + monthlySalary
				+ ", temporaryPermanent=" + temporaryPermanent + ", experienceCertificate=" + experienceCertificate
				+ "]";
	}
	
}
