package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "job_amount_details")
public class JobAmountDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "amount")
	private int amount;

	@Column(name = "post_number")
	private int postNumber;

	@Column(name = "min_age")
	private int minAge;

	@Column(name = "max_age")
	private int maxAge;
	
	private String minAgeDate;
	private String maxAgeDate;

	@Column(name = "age_relax")
	private int ageRelaxation;

	@Column(name = "percent_relax")
	private int percentageRelaxation;

	@Column(name = "category_name")
	private String categoryName;

//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "viewid")
//	private RegistrationViewBean viewBean;
	
	@ManyToOne
	@JoinColumn(name="view_id")
	private RegistrationViewBean viewBean;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private JobDetails jobDetails;

	public JobAmountDetails() {
		super();
	}

	public JobAmountDetails(Long id, int amount, int postNumber, int minAge, int maxAge, String minAgeDate,
			String maxAgeDate, int ageRelaxation, int percentageRelaxation, String categoryName,
			RegistrationViewBean viewBean, JobDetails jobDetails) {
		super();
		this.id = id;
		this.amount = amount;
		this.postNumber = postNumber;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.minAgeDate = minAgeDate;
		this.maxAgeDate = maxAgeDate;
		this.ageRelaxation = ageRelaxation;
		this.percentageRelaxation = percentageRelaxation;
		this.categoryName = categoryName;
		this.viewBean = viewBean;
		this.jobDetails = jobDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getPostNumber() {
		return postNumber;
	}

	public void setPostNumber(int postNumber) {
		this.postNumber = postNumber;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public String getMinAgeDate() {
		return minAgeDate;
	}

	public void setMinAgeDate(String minAgeDate) {
		this.minAgeDate = minAgeDate;
	}

	public String getMaxAgeDate() {
		return maxAgeDate;
	}

	public void setMaxAgeDate(String maxAgeDate) {
		this.maxAgeDate = maxAgeDate;
	}

	public int getAgeRelaxation() {
		return ageRelaxation;
	}

	public void setAgeRelaxation(int ageRelaxation) {
		this.ageRelaxation = ageRelaxation;
	}

	public int getPercentageRelaxation() {
		return percentageRelaxation;
	}

	public void setPercentageRelaxation(int percentageRelaxation) {
		this.percentageRelaxation = percentageRelaxation;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public RegistrationViewBean getViewBean() {
		return viewBean;
	}

	public void setViewBean(RegistrationViewBean viewBean) {
		this.viewBean = viewBean;
	}

	public JobDetails getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(JobDetails jobDetails) {
		this.jobDetails = jobDetails;
	}

	@Override
	public String toString() {
		return "JobAmountDetails [id=" + id + ", amount=" + amount + ", postNumber=" + postNumber + ", minAge=" + minAge
				+ ", maxAge=" + maxAge + ", minAgeDate=" + minAgeDate + ", maxAgeDate=" + maxAgeDate
				+ ", ageRelaxation=" + ageRelaxation + ", percentageRelaxation=" + percentageRelaxation
				+ ", categoryName=" + categoryName + ", viewBean=" + viewBean + ", jobDetails=" + jobDetails + "]";
	}
}
