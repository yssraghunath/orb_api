package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_recruitment_zone")
public class RecruitmentZone implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "rishikesh_zone")
	private boolean rishikeshZone;

	@Column(name = "pune_zone")
	private boolean puneZone;

	@Column(name = "tezpur_zone")
	private boolean tezpurZone;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public RecruitmentZone() {
		super();
	}

	public RecruitmentZone(int id, boolean rishikeshZone, boolean puneZone, boolean tezpurZone,
			RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.rishikeshZone = rishikeshZone;
		this.puneZone = puneZone;
		this.tezpurZone = tezpurZone;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isRishikeshZone() {
		return rishikeshZone;
	}

	public void setRishikeshZone(boolean rishikeshZone) {
		this.rishikeshZone = rishikeshZone;
	}

	public boolean isPuneZone() {
		return puneZone;
	}

	public void setPuneZone(boolean puneZone) {
		this.puneZone = puneZone;
	}

	public boolean isTezpurZone() {
		return tezpurZone;
	}

	public void setTezpurZone(boolean tezpurZone) {
		this.tezpurZone = tezpurZone;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "RecruitmentZone [id=" + id + ", rishikeshZone=" + rishikeshZone + ", puneZone=" + puneZone
				+ ", tezpurZone=" + tezpurZone + ", registrationViewBean=" + registrationViewBean + "]";
	}
	
	
}
