package com.bro.orb.domain;


public class JobsFormDescription {

	private String jobTitle;
	private String jobDesc;

	public JobsFormDescription() {
		super();
	}

	public JobsFormDescription(String jobTitle, String jobDesc) {
		super();
		this.jobTitle = jobTitle;
		this.jobDesc = jobDesc;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	@Override
	public String toString() {
		return "JobsFormDescription [jobTitle=" + jobTitle + ", jobDesc="
				+ jobDesc + "]";
	}

}
