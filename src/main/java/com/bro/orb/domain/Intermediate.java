package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_intermediate")
public class Intermediate implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "inter_year")
	private boolean interYearView;

	@Column(name = "inter_institution")
	private boolean interInstitutionView;

	@Column(name = "inter_specialization")
	private boolean interSpecializationView;

	@Column(name = "inter_course_type")
	private boolean interCourseTypeView;

	@Column(name = "inter_total_marks")
	private boolean interTotalMarksView;

	@Column(name = "inter_obtain_marks")
	private boolean interObtainarksView;

	@Column(name = "inter_percentage")
	private boolean interPercentageView;

	@Column(name = "inter_board")
	private boolean interBoardView;
	
	@Column(name = "inter_grade")
	private boolean interGradeView;

	@Column(name = "inter_certificate")
	private boolean interCertificateView;

	@Column(name = "inter_min_percent")
	private int interMinPercent;

	@Column(name = "additional_view")
	private boolean additionalCertificateView;
	
	@Column(name = "firstaid_view")
	private boolean FirstaidCertificateView;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public Intermediate() {
		super();
	}

	public Intermediate(int id, boolean interYearView, boolean interInstitutionView, boolean interSpecializationView,
			boolean interCourseTypeView, boolean interTotalMarksView, boolean interObtainarksView,
			boolean interPercentageView, boolean interBoardView, boolean interGradeView, boolean interCertificateView,
			int interMinPercent, boolean additionalCertificateView, boolean firstaidCertificateView,
			RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.interYearView = interYearView;
		this.interInstitutionView = interInstitutionView;
		this.interSpecializationView = interSpecializationView;
		this.interCourseTypeView = interCourseTypeView;
		this.interTotalMarksView = interTotalMarksView;
		this.interObtainarksView = interObtainarksView;
		this.interPercentageView = interPercentageView;
		this.interBoardView = interBoardView;
		this.interGradeView = interGradeView;
		this.interCertificateView = interCertificateView;
		this.interMinPercent = interMinPercent;
		this.additionalCertificateView = additionalCertificateView;
		FirstaidCertificateView = firstaidCertificateView;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isInterYearView() {
		return interYearView;
	}

	public void setInterYearView(boolean interYearView) {
		this.interYearView = interYearView;
	}

	public boolean isInterInstitutionView() {
		return interInstitutionView;
	}

	public void setInterInstitutionView(boolean interInstitutionView) {
		this.interInstitutionView = interInstitutionView;
	}

	public boolean isInterSpecializationView() {
		return interSpecializationView;
	}

	public void setInterSpecializationView(boolean interSpecializationView) {
		this.interSpecializationView = interSpecializationView;
	}

	public boolean isInterCourseTypeView() {
		return interCourseTypeView;
	}

	public void setInterCourseTypeView(boolean interCourseTypeView) {
		this.interCourseTypeView = interCourseTypeView;
	}

	public boolean isInterTotalMarksView() {
		return interTotalMarksView;
	}

	public void setInterTotalMarksView(boolean interTotalMarksView) {
		this.interTotalMarksView = interTotalMarksView;
	}

	public boolean isInterObtainarksView() {
		return interObtainarksView;
	}

	public void setInterObtainarksView(boolean interObtainarksView) {
		this.interObtainarksView = interObtainarksView;
	}

	public boolean isInterPercentageView() {
		return interPercentageView;
	}

	public void setInterPercentageView(boolean interPercentageView) {
		this.interPercentageView = interPercentageView;
	}

	public boolean isInterBoardView() {
		return interBoardView;
	}

	public void setInterBoardView(boolean interBoardView) {
		this.interBoardView = interBoardView;
	}

	public boolean isInterGradeView() {
		return interGradeView;
	}

	public void setInterGradeView(boolean interGradeView) {
		this.interGradeView = interGradeView;
	}

	public boolean isInterCertificateView() {
		return interCertificateView;
	}

	public void setInterCertificateView(boolean interCertificateView) {
		this.interCertificateView = interCertificateView;
	}

	public int getInterMinPercent() {
		return interMinPercent;
	}

	public void setInterMinPercent(int interMinPercent) {
		this.interMinPercent = interMinPercent;
	}

	public boolean isAdditionalCertificateView() {
		return additionalCertificateView;
	}

	public void setAdditionalCertificateView(boolean additionalCertificateView) {
		this.additionalCertificateView = additionalCertificateView;
	}

	public boolean isFirstaidCertificateView() {
		return FirstaidCertificateView;
	}

	public void setFirstaidCertificateView(boolean firstaidCertificateView) {
		FirstaidCertificateView = firstaidCertificateView;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "Intermediate [id=" + id + ", interYearView=" + interYearView + ", interInstitutionView="
				+ interInstitutionView + ", interSpecializationView=" + interSpecializationView
				+ ", interCourseTypeView=" + interCourseTypeView + ", interTotalMarksView=" + interTotalMarksView
				+ ", interObtainarksView=" + interObtainarksView + ", interPercentageView=" + interPercentageView
				+ ", interBoardView=" + interBoardView + ", interGradeView=" + interGradeView
				+ ", interCertificateView=" + interCertificateView + ", interMinPercent=" + interMinPercent
				+ ", additionalCertificateView=" + additionalCertificateView + ", FirstaidCertificateView="
				+ FirstaidCertificateView + ", registrationViewBean=" + registrationViewBean + "]";
	}

	
		
}
