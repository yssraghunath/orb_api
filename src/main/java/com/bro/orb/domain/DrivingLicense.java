package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bro_candidate_driving_license")
public class DrivingLicense extends AbstractAuditingEntity implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	
	@Column(name = "licence_number_lmv")
	private String licenceNumberLmv;
	
	
	@Column(name = "issue_date_lmv")
	private Date issueDateLmv;
	
	
	@Column(name = "valid_upto_lmv")
	private Date validUptoLmv;
	
	
	@Column(name = "issuing_authority_lmv")
	private String issuingAuthorityLmv;
	
	
	@Column(name = "licence_number_hgmv")
	private String licenceNumberHgmv;
	
	
	@Column(name = "issue_date_hgmv")
	private Date issueDateHgmv;
	
	
	@Column(name = "valid_upto_hgmv")
	private Date validUptoHgmv;
	
	
	@Column(name = "issuing_authority_hlmv")
	private String issuingAuthorityHlmv;
	
	@Column(name = "upload_licence")
	private String uploadLicence;

	@Column(name = "driving_experience")
	private String drivingExperience;
	
	@Column(name = "licence_number_road")
	private String licenceNumberRoad;
	
	
	@Column(name = "issue_date_road")
	private Date issueDateRoad;
	
	
	@Column(name = "valid_upto_road")
	private Date validUptoRoad;
	
	
	@Column(name = "issuing_authority_road")
	private String issuingAuthorityRoad;
	
	@Column(name = "licence_number_oeg")
	private String licenceNumberOeg;
	
	
	@Column(name = "issue_date_oeg")
	private Date issueDateOeg;
	
	
	@Column(name = "valid_upto_oeg")
	private Date validUptoOeg;
	
	
	@Column(name = "issuing_authority_oeg")
	private String issuingAuthorityOeg;
	

	
	public DrivingLicense() {
		super();
	}



	public DrivingLicense(long id, String licenceNumberLmv, Date issueDateLmv, Date validUptoLmv,
			String issuingAuthorityLmv, String licenceNumberHgmv, Date issueDateHgmv, Date validUptoHgmv,
			String issuingAuthorityHlmv, String uploadLicence, String drivingExperience, String licenceNumberRoad,
			Date issueDateRoad, Date validUptoRoad, String issuingAuthorityRoad, String licenceNumberOeg,
			Date issueDateOeg, Date validUptoOeg, String issuingAuthorityOeg) {
		super();
		this.id = id;
		this.licenceNumberLmv = licenceNumberLmv;
		this.issueDateLmv = issueDateLmv;
		this.validUptoLmv = validUptoLmv;
		this.issuingAuthorityLmv = issuingAuthorityLmv;
		this.licenceNumberHgmv = licenceNumberHgmv;
		this.issueDateHgmv = issueDateHgmv;
		this.validUptoHgmv = validUptoHgmv;
		this.issuingAuthorityHlmv = issuingAuthorityHlmv;
		this.uploadLicence = uploadLicence;
		this.drivingExperience = drivingExperience;
		this.licenceNumberRoad = licenceNumberRoad;
		this.issueDateRoad = issueDateRoad;
		this.validUptoRoad = validUptoRoad;
		this.issuingAuthorityRoad = issuingAuthorityRoad;
		this.licenceNumberOeg = licenceNumberOeg;
		this.issueDateOeg = issueDateOeg;
		this.validUptoOeg = validUptoOeg;
		this.issuingAuthorityOeg = issuingAuthorityOeg;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getLicenceNumberLmv() {
		return licenceNumberLmv;
	}



	public void setLicenceNumberLmv(String licenceNumberLmv) {
		this.licenceNumberLmv = licenceNumberLmv;
	}



	public Date getIssueDateLmv() {
		return issueDateLmv;
	}



	public void setIssueDateLmv(Date issueDateLmv) {
		this.issueDateLmv = issueDateLmv;
	}



	public Date getValidUptoLmv() {
		return validUptoLmv;
	}



	public void setValidUptoLmv(Date validUptoLmv) {
		this.validUptoLmv = validUptoLmv;
	}



	public String getIssuingAuthorityLmv() {
		return issuingAuthorityLmv;
	}



	public void setIssuingAuthorityLmv(String issuingAuthorityLmv) {
		this.issuingAuthorityLmv = issuingAuthorityLmv;
	}



	public String getLicenceNumberHgmv() {
		return licenceNumberHgmv;
	}



	public void setLicenceNumberHgmv(String licenceNumberHgmv) {
		this.licenceNumberHgmv = licenceNumberHgmv;
	}



	public Date getIssueDateHgmv() {
		return issueDateHgmv;
	}



	public void setIssueDateHgmv(Date issueDateHgmv) {
		this.issueDateHgmv = issueDateHgmv;
	}



	public Date getValidUptoHgmv() {
		return validUptoHgmv;
	}



	public void setValidUptoHgmv(Date validUptoHgmv) {
		this.validUptoHgmv = validUptoHgmv;
	}



	public String getIssuingAuthorityHlmv() {
		return issuingAuthorityHlmv;
	}



	public void setIssuingAuthorityHlmv(String issuingAuthorityHlmv) {
		this.issuingAuthorityHlmv = issuingAuthorityHlmv;
	}



	public String getUploadLicence() {
		return uploadLicence;
	}



	public void setUploadLicence(String uploadLicence) {
		this.uploadLicence = uploadLicence;
	}



	public String getDrivingExperience() {
		return drivingExperience;
	}



	public void setDrivingExperience(String drivingExperience) {
		this.drivingExperience = drivingExperience;
	}



	public String getLicenceNumberRoad() {
		return licenceNumberRoad;
	}



	public void setLicenceNumberRoad(String licenceNumberRoad) {
		this.licenceNumberRoad = licenceNumberRoad;
	}



	public Date getIssueDateRoad() {
		return issueDateRoad;
	}



	public void setIssueDateRoad(Date issueDateRoad) {
		this.issueDateRoad = issueDateRoad;
	}



	public Date getValidUptoRoad() {
		return validUptoRoad;
	}



	public void setValidUptoRoad(Date validUptoRoad) {
		this.validUptoRoad = validUptoRoad;
	}



	public String getIssuingAuthorityRoad() {
		return issuingAuthorityRoad;
	}



	public void setIssuingAuthorityRoad(String issuingAuthorityRoad) {
		this.issuingAuthorityRoad = issuingAuthorityRoad;
	}



	public String getLicenceNumberOeg() {
		return licenceNumberOeg;
	}



	public void setLicenceNumberOeg(String licenceNumberOeg) {
		this.licenceNumberOeg = licenceNumberOeg;
	}



	public Date getIssueDateOeg() {
		return issueDateOeg;
	}



	public void setIssueDateOeg(Date issueDateOeg) {
		this.issueDateOeg = issueDateOeg;
	}



	public Date getValidUptoOeg() {
		return validUptoOeg;
	}



	public void setValidUptoOeg(Date validUptoOeg) {
		this.validUptoOeg = validUptoOeg;
	}



	public String getIssuingAuthorityOeg() {
		return issuingAuthorityOeg;
	}



	public void setIssuingAuthorityOeg(String issuingAuthorityOeg) {
		this.issuingAuthorityOeg = issuingAuthorityOeg;
	}



	@Override
	public String toString() {
		return "DrivingLicense [id=" + id + ", licenceNumberLmv=" + licenceNumberLmv + ", issueDateLmv=" + issueDateLmv
				+ ", validUptoLmv=" + validUptoLmv + ", issuingAuthorityLmv=" + issuingAuthorityLmv
				+ ", licenceNumberHgmv=" + licenceNumberHgmv + ", issueDateHgmv=" + issueDateHgmv + ", validUptoHgmv="
				+ validUptoHgmv + ", issuingAuthorityHlmv=" + issuingAuthorityHlmv + ", uploadLicence=" + uploadLicence
				+ ", drivingExperience=" + drivingExperience + ", licenceNumberRoad=" + licenceNumberRoad
				+ ", issueDateRoad=" + issueDateRoad + ", validUptoRoad=" + validUptoRoad + ", issuingAuthorityRoad="
				+ issuingAuthorityRoad + ", licenceNumberOeg=" + licenceNumberOeg + ", issueDateOeg=" + issueDateOeg
				+ ", validUptoOeg=" + validUptoOeg + ", issuingAuthorityOeg=" + issuingAuthorityOeg + "]";
	}



		
}
