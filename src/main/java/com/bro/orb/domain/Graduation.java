package com.bro.orb.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "bro_graduate")
public class Graduation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "ug_year")
	private boolean ugYearView;

	@Column(name = "ug_institution")
	private boolean ugInstitutionView;

	@Column(name = "ug_specialization")
	private boolean ugSpecializationView;

	@Column(name = "ug_course_type")
	private boolean ugCourseTypeView;

	@Column(name = "ug_total_marks")
	private boolean ugTotalMarksView;

	@Column(name = "ug_obtain_marks")
	private boolean ugObtainarksView;

	@Column(name = "ug_percentage")
	private boolean ugPercentageView;

	@Column(name = "ug_course")
	private boolean ugCourseView;

	@Column(name = "ug_university")
	private boolean ugUniversityView;

	@Column(name = "ug_certificate")
	private boolean ugCertificateView;
	
	@Column(name = "ug_min_percent")
	private int ugMinPercent;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public Graduation() {
		super();
	}

	public Graduation(int id, boolean ugYearView, boolean ugInstitutionView, boolean ugSpecializationView,
			boolean ugCourseTypeView, boolean ugTotalMarksView, boolean ugObtainarksView, boolean ugPercentageView,
			boolean ugCourseView, boolean ugUniversityView, boolean ugCertificateView, int ugMinPercent,
			RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.ugYearView = ugYearView;
		this.ugInstitutionView = ugInstitutionView;
		this.ugSpecializationView = ugSpecializationView;
		this.ugCourseTypeView = ugCourseTypeView;
		this.ugTotalMarksView = ugTotalMarksView;
		this.ugObtainarksView = ugObtainarksView;
		this.ugPercentageView = ugPercentageView;
		this.ugCourseView = ugCourseView;
		this.ugUniversityView = ugUniversityView;
		this.ugCertificateView = ugCertificateView;
		this.ugMinPercent = ugMinPercent;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isUgYearView() {
		return ugYearView;
	}

	public void setUgYearView(boolean ugYearView) {
		this.ugYearView = ugYearView;
	}

	public boolean isUgInstitutionView() {
		return ugInstitutionView;
	}

	public void setUgInstitutionView(boolean ugInstitutionView) {
		this.ugInstitutionView = ugInstitutionView;
	}

	public boolean isUgSpecializationView() {
		return ugSpecializationView;
	}

	public void setUgSpecializationView(boolean ugSpecializationView) {
		this.ugSpecializationView = ugSpecializationView;
	}

	public boolean isUgCourseTypeView() {
		return ugCourseTypeView;
	}

	public void setUgCourseTypeView(boolean ugCourseTypeView) {
		this.ugCourseTypeView = ugCourseTypeView;
	}

	public boolean isUgTotalMarksView() {
		return ugTotalMarksView;
	}

	public void setUgTotalMarksView(boolean ugTotalMarksView) {
		this.ugTotalMarksView = ugTotalMarksView;
	}

	public boolean isUgObtainarksView() {
		return ugObtainarksView;
	}

	public void setUgObtainarksView(boolean ugObtainarksView) {
		this.ugObtainarksView = ugObtainarksView;
	}

	public boolean isUgPercentageView() {
		return ugPercentageView;
	}

	public void setUgPercentageView(boolean ugPercentageView) {
		this.ugPercentageView = ugPercentageView;
	}

	public boolean isUgCourseView() {
		return ugCourseView;
	}

	public void setUgCourseView(boolean ugCourseView) {
		this.ugCourseView = ugCourseView;
	}

	public boolean isUgUniversityView() {
		return ugUniversityView;
	}

	public void setUgUniversityView(boolean ugUniversityView) {
		this.ugUniversityView = ugUniversityView;
	}

	public boolean isUgCertificateView() {
		return ugCertificateView;
	}

	public void setUgCertificateView(boolean ugCertificateView) {
		this.ugCertificateView = ugCertificateView;
	}

	public int getUgMinPercent() {
		return ugMinPercent;
	}

	public void setUgMinPercent(int ugMinPercent) {
		this.ugMinPercent = ugMinPercent;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "Graduation [id=" + id + ", ugYearView=" + ugYearView + ", ugInstitutionView=" + ugInstitutionView
				+ ", ugSpecializationView=" + ugSpecializationView + ", ugCourseTypeView=" + ugCourseTypeView
				+ ", ugTotalMarksView=" + ugTotalMarksView + ", ugObtainarksView=" + ugObtainarksView
				+ ", ugPercentageView=" + ugPercentageView + ", ugCourseView=" + ugCourseView + ", ugUniversityView="
				+ ugUniversityView + ", ugCertificateView=" + ugCertificateView + ", ugMinPercent=" + ugMinPercent
				+ ", registrationViewBean=" + registrationViewBean + "]";
	}
}
