package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="bro_shortlist1_upload")
public class ShortListAUploadCertificate extends  AbstractAuditingEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(name="browse_photo")
	private String browsePhoto;
	
	
	@Column(name="browse_signature")
	private String browseSignature;
	
	public ShortListAUploadCertificate(long id, String browsePhoto, String browseSignature) {
		super();
		this.id = id;
		this.browsePhoto = browsePhoto;
		this.browseSignature = browseSignature;
	}
	public ShortListAUploadCertificate() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getBrowsePhoto() {
		return browsePhoto;
	}
	public void setBrowsePhoto(String browsePhoto) {
		this.browsePhoto = browsePhoto;
	}
	public String getBrowseSignature() {
		return browseSignature;
	}
	public void setBrowseSignature(String browseSignature) {
		this.browseSignature = browseSignature;
	}
	
	@Override
	public String toString() {
		return "UploadCertificate [id=" + id + ", browsePhoto=" + browsePhoto + ", browseSignature=" + browseSignature
				+ "]";
	}
}
