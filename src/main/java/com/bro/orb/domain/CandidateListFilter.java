package com.bro.orb.domain;

public class CandidateListFilter {

	private String registrationId;
	private String candidateName;
	private String postName;
	private String category;
	private String qualification;
	private String percentage;
	private String grade;
	private boolean nccb;
	private boolean nccc;
	private boolean disable;
	private String nonindian;
	private boolean sportsman;
	private boolean sondaughterEsm;
	private boolean sondaughterGref;
	private boolean brosister;
	private boolean centrakgov;
	private boolean age;

	
	
	
	
	public void setAge(boolean age) {
		this.age = age;
	}

	public boolean isAge() {
		return age;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public boolean isNccb() {
		return nccb;
	}

	public void setNccb(boolean nccb) {
		this.nccb = nccb;
	}

	public boolean isNccc() {
		return nccc;
	}

	public void setNccc(boolean nccc) {
		this.nccc = nccc;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public String getNonindian() {
		return nonindian;
	}

	public void setNonindian(String nonindian) {
		this.nonindian = nonindian;
	}

	public boolean isSportsman() {
		return sportsman;
	}

	public void setSportsman(boolean sportsman) {
		this.sportsman = sportsman;
	}

	public boolean isSondaughterEsm() {
		return sondaughterEsm;
	}

	public void setSondaughterEsm(boolean sondaughterEsm) {
		this.sondaughterEsm = sondaughterEsm;
	}

	public boolean isSondaughterGref() {
		return sondaughterGref;
	}

	public void setSondaughterGref(boolean sondaughterGref) {
		this.sondaughterGref = sondaughterGref;
	}

	public boolean isBrosister() {
		return brosister;
	}

	public void setBrosister(boolean brosister) {
		this.brosister = brosister;
	}

	public boolean isCentrakgov() {
		return centrakgov;
	}

	public void setCentrakgov(boolean centrakgov) {
		this.centrakgov = centrakgov;
	}

	

		
}
