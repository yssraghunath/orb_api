package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_exam_center")
public class ExaminationCenter extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "state_name")
	private String stateName;

	@Column(name = "city_name")
	private String cityName;

	@Column(name = "center_name")
	private String centerName;

	private String centerAddress;

	@Column(name = "available_seats")
	private int availableSeats;

	@Column(name = "zoneName")
	private int zoneName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private CenterAddress address;

	public ExaminationCenter() {
		super();
	}

	public ExaminationCenter(int id, String stateName, String cityName, String centerName, String centerAddress,
			int availableSeats, int zoneName, CenterAddress address) {
		super();
		this.id = id;
		this.stateName = stateName;
		this.cityName = cityName;
		this.centerName = centerName;
		this.centerAddress = centerAddress;
		this.availableSeats = availableSeats;
		this.zoneName = zoneName;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getCenterAddress() {
		return centerAddress;
	}

	public void setCenterAddress(String centerAddress) {
		this.centerAddress = centerAddress;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public int getZoneName() {
		return zoneName;
	}

	public void setZoneName(int zoneName) {
		this.zoneName = zoneName;
	}

	public CenterAddress getAddress() {
		return address;
	}

	public void setAddress(CenterAddress address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "ExaminationCenter [id=" + id + ", stateName=" + stateName + ", cityName=" + cityName + ", centerName="
				+ centerName + ", centerAddress=" + centerAddress + ", availableSeats=" + availableSeats + ", zoneName="
				+ zoneName + ", address=" + address + "]";
	}
}
