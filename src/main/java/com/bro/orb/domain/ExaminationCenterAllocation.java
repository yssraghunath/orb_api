package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_center_allocation")
public class ExaminationCenterAllocation extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "center_name")
	private String centerName;

	@Column(name = "registration_id_from")
	private String registrationIdFrom;

	@Column(name = "registration_id_to")
	private int registrationIdTo;

	@Column(name = "zoneName")
	private String zoneName;
	
	@Column (name="Exam_date")
	private String examDate;
	
	@Column(name="Exam_Time")
	private String examTime;
	
	@Column(name="Paper_Duration")
	private String duration;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private JobDetails jobDetails;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "center_id", referencedColumnName = "id")
	private ExaminationCenter examinationCenter;

	public ExaminationCenterAllocation() {
		super();
	}

	public ExaminationCenterAllocation(int id, String centerName, String registrationIdFrom, int registrationIdTo,
			String zoneName, String examDate, String examTime, String duration, JobDetails jobDetails,
			ExaminationCenter examinationCenter) {
		super();
		this.id = id;
		this.centerName = centerName;
		this.registrationIdFrom = registrationIdFrom;
		this.registrationIdTo = registrationIdTo;
		this.zoneName = zoneName;
		this.examDate = examDate;
		this.examTime = examTime;
		this.duration = duration;
		this.jobDetails = jobDetails;
		this.examinationCenter = examinationCenter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getRegistrationIdFrom() {
		return registrationIdFrom;
	}

	public void setRegistrationIdFrom(String registrationIdFrom) {
		this.registrationIdFrom = registrationIdFrom;
	}

	public int getRegistrationIdTo() {
		return registrationIdTo;
	}

	public void setRegistrationIdTo(int registrationIdTo) {
		this.registrationIdTo = registrationIdTo;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public String getExamTime() {
		return examTime;
	}

	public void setExamTime(String examTime) {
		this.examTime = examTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public JobDetails getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(JobDetails jobDetails) {
		this.jobDetails = jobDetails;
	}

	public ExaminationCenter getExaminationCenter() {
		return examinationCenter;
	}

	public void setExaminationCenter(ExaminationCenter examinationCenter) {
		this.examinationCenter = examinationCenter;
	}

	@Override
	public String toString() {
		return "ExaminationCenterAllocation [id=" + id + ", centerName=" + centerName + ", registrationIdFrom="
				+ registrationIdFrom + ", registrationIdTo=" + registrationIdTo + ", zoneName=" + zoneName
				+ ", examDate=" + examDate + ", examTime=" + examTime + ", duration=" + duration + ", jobDetails="
				+ jobDetails + ", examinationCenter=" + examinationCenter + "]";
	}


	
}
