package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bro_candidate_ug_pg_details")
public class GraduationQualification extends AbstractAuditingEntity implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	
	@Column(name = "ug_course" )
	private String ugCourse;
	
	
	@Column(name = "ug_passing_year")
	private int ugPassingYear;
	
	
	@Column(name = "ug_institution")
	private String ugInstitution;
	
	
	@Column(name = "ug_university")
	private String ugUniversity;
	
	
	@Column(name = "ug_specialization")
	private String ugSpecialization;
	
	
	@Column(name = "ug_total_marks")
	private int ugTotalMarks;
	
	
	@Column(name = "ug_obtain_marks")
	private int ugObtainMarks;
	
	
	
	@Column(name = "ug_percentage")
	private float ugPercentage;

	
	@Column(name = "ug_course_type")
    private String ugCourseType;
	
	
	@Column(name = "ug_upload_certificate")
	private String ugCertificate;
	
	
	@Column(name = "pg_course")
	private String pgCourse;
	
	
	@Column(name = "pg_passing_year")
	private int pgPassingYear;
	
	
	@Column(name = "pg_institution")
	private String pgInstitution;
	
	
	@Column(name = "pg_university")
	private String pgUniversity;
	
	
	@Column(name = "pg_specialization")
	private String pgSpecialization;
	
	
	@Column(name = "pg_total_marks")
	private int pgTotalMarks;
	
	
	@Column(name = "pg_obtain_marks")
	private int pgObtainMarks;
	
	
	@Column(name = "pg_percentage")
	private float pgPercentage;
	
	
	@Column(name = "pg_course_type")
	private String pgCourseType;
	
	@Column(name = "pg_upload_certificate")
	private String pgCertificate;

	
	public GraduationQualification() {
		super();
	}


	public GraduationQualification(long id, String ugCourse, int ugPassingYear, String ugInstitution,
			String ugUniversity, String ugSpecialization, int ugTotalMarks, int ugObtainMarks, float ugPercentage,
			String ugCourseType, String ugCertificate, String pgCourse, int pgPassingYear, String pgInstitution,
			String pgUniversity, String pgSpecialization, int pgTotalMarks, int pgObtainMarks, float pgPercentage,
			String pgCourseType, String pgCertificate) {
		super();
		this.id = id;
		this.ugCourse = ugCourse;
		this.ugPassingYear = ugPassingYear;
		this.ugInstitution = ugInstitution;
		this.ugUniversity = ugUniversity;
		this.ugSpecialization = ugSpecialization;
		this.ugTotalMarks = ugTotalMarks;
		this.ugObtainMarks = ugObtainMarks;
		this.ugPercentage = ugPercentage;
		this.ugCourseType = ugCourseType;
		this.ugCertificate = ugCertificate;
		this.pgCourse = pgCourse;
		this.pgPassingYear = pgPassingYear;
		this.pgInstitution = pgInstitution;
		this.pgUniversity = pgUniversity;
		this.pgSpecialization = pgSpecialization;
		this.pgTotalMarks = pgTotalMarks;
		this.pgObtainMarks = pgObtainMarks;
		this.pgPercentage = pgPercentage;
		this.pgCourseType = pgCourseType;
		this.pgCertificate = pgCertificate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUgCourse() {
		return ugCourse;
	}


	public void setUgCourse(String ugCourse) {
		this.ugCourse = ugCourse;
	}


	public int getUgPassingYear() {
		return ugPassingYear;
	}


	public void setUgPassingYear(int ugPassingYear) {
		this.ugPassingYear = ugPassingYear;
	}


	public String getUgInstitution() {
		return ugInstitution;
	}


	public void setUgInstitution(String ugInstitution) {
		this.ugInstitution = ugInstitution;
	}


	public String getUgUniversity() {
		return ugUniversity;
	}


	public void setUgUniversity(String ugUniversity) {
		this.ugUniversity = ugUniversity;
	}


	public String getUgSpecialization() {
		return ugSpecialization;
	}


	public void setUgSpecialization(String ugSpecialization) {
		this.ugSpecialization = ugSpecialization;
	}


	public int getUgTotalMarks() {
		return ugTotalMarks;
	}


	public void setUgTotalMarks(int ugTotalMarks) {
		this.ugTotalMarks = ugTotalMarks;
	}


	public int getUgObtainMarks() {
		return ugObtainMarks;
	}


	public void setUgObtainMarks(int ugObtainMarks) {
		this.ugObtainMarks = ugObtainMarks;
	}


	public float getUgPercentage() {
		return ugPercentage;
	}


	public void setUgPercentage(float ugPercentage) {
		this.ugPercentage = ugPercentage;
	}


	public String getUgCourseType() {
		return ugCourseType;
	}


	public void setUgCourseType(String ugCourseType) {
		this.ugCourseType = ugCourseType;
	}


	public String getUgCertificate() {
		return ugCertificate;
	}


	public void setUgCertificate(String ugCertificate) {
		this.ugCertificate = ugCertificate;
	}


	public String getPgCourse() {
		return pgCourse;
	}


	public void setPgCourse(String pgCourse) {
		this.pgCourse = pgCourse;
	}


	public int getPgPassingYear() {
		return pgPassingYear;
	}


	public void setPgPassingYear(int pgPassingYear) {
		this.pgPassingYear = pgPassingYear;
	}


	public String getPgInstitution() {
		return pgInstitution;
	}


	public void setPgInstitution(String pgInstitution) {
		this.pgInstitution = pgInstitution;
	}


	public String getPgUniversity() {
		return pgUniversity;
	}


	public void setPgUniversity(String pgUniversity) {
		this.pgUniversity = pgUniversity;
	}


	public String getPgSpecialization() {
		return pgSpecialization;
	}


	public void setPgSpecialization(String pgSpecialization) {
		this.pgSpecialization = pgSpecialization;
	}


	public int getPgTotalMarks() {
		return pgTotalMarks;
	}


	public void setPgTotalMarks(int pgTotalMarks) {
		this.pgTotalMarks = pgTotalMarks;
	}


	public int getPgObtainMarks() {
		return pgObtainMarks;
	}


	public void setPgObtainMarks(int pgObtainMarks) {
		this.pgObtainMarks = pgObtainMarks;
	}


	public float getPgPercentage() {
		return pgPercentage;
	}


	public void setPgPercentage(float pgPercentage) {
		this.pgPercentage = pgPercentage;
	}


	public String getPgCourseType() {
		return pgCourseType;
	}


	public void setPgCourseType(String pgCourseType) {
		this.pgCourseType = pgCourseType;
	}


	public String getPgCertificate() {
		return pgCertificate;
	}


	public void setPgCertificate(String pgCertificate) {
		this.pgCertificate = pgCertificate;
	}


	@Override
	public String toString() {
		return "GraduationQualification [id=" + id + ", ugCourse=" + ugCourse + ", ugPassingYear=" + ugPassingYear
				+ ", ugInstitution=" + ugInstitution + ", ugUniversity=" + ugUniversity + ", ugSpecialization="
				+ ugSpecialization + ", ugTotalMarks=" + ugTotalMarks + ", ugObtainMarks=" + ugObtainMarks
				+ ", ugPercentage=" + ugPercentage + ", ugCourseType=" + ugCourseType + ", ugCertificate="
				+ ugCertificate + ", pgCourse=" + pgCourse + ", pgPassingYear=" + pgPassingYear + ", pgInstitution="
				+ pgInstitution + ", pgUniversity=" + pgUniversity + ", pgSpecialization=" + pgSpecialization
				+ ", pgTotalMarks=" + pgTotalMarks + ", pgObtainMarks=" + pgObtainMarks + ", pgPercentage="
				+ pgPercentage + ", pgCourseType=" + pgCourseType + ", pgCertificate=" + pgCertificate + "]";
	}
	
	}
