package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "bro_candidate_essential_qualification")
public class EssentialQualification extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "hss_passing_year")
	private int highYear;

	@Column(name = "hss_institution")
	private String highInstitution;

	@Column(name = "hss_specialization")
	private String highSpecialization;

	@Column(name = "hss_course_type")
	private String highCourseType;

	@Column(name = "hss_board")
	private String highBoard;

	@Column(name = "hss_total_marks")
	private int highTotalMarks;

	@Column(name = "hss_marks_obtain")
	private int highObtainMarks;

	@Column(name = "hss_percentage")
	private float highPercentage;

	@Column(name = "hss_certificate")
	private String highCertificate;

	@Column(name = "hss_grade")
	private String highGrade;

	@Column(name = "sss_grade")
	private String interGrade;

	@Column(name = "sss_passing_year")
	private int interYear;

	@Column(name = "sss_institution")
	private String interInstitution;

	@Column(name = "sss_specialization")
	private String interSpecialization;

	@Column(name = "sss_course_type")
	private String interCourseType;

	@Column(name = "sss_board")
	private String interBoard;

	@Column(name = "sss_total_marks")
	private int interTotalMarks;

	@Column(name = "sss_marks_obtain")
	private int interObtainMarks;

	@Column(name = "sss_percentage")
	private float interPercentage;

	@Column(name = "sss_certificate")
	private String interCertificate;

	@Column(name = "english_typing_speed")
	private int englishTyping;

	@Column(name = "hindi_typing_speed")
	private int hindiTyping;

	@Column(name = "additional_certificate")
	private String additionalCertificate;
	
	@Column(name = "english_stenography_speed")
	private int englishStenography;

	@Column(name = "hindi_stenography_speed")
	private int hindiStenography;

	@Column(name = "firstaid_certificate")
	private String firstaidCertificate;

	public EssentialQualification() {
		super();
	}

	public EssentialQualification(long id, int highYear, String highInstitution, String highSpecialization,
			String highCourseType, String highBoard, int highTotalMarks, int highObtainMarks, float highPercentage,
			String highCertificate, String highGrade, String interGrade, int interYear, String interInstitution,
			String interSpecialization, String interCourseType, String interBoard, int interTotalMarks,
			int interObtainMarks, float interPercentage, String interCertificate, int englishTyping, int hindiTyping,
			String additionalCertificate, int englishStenography, int hindiStenography, String firstaidCertificate) {
		super();
		this.id = id;
		this.highYear = highYear;
		this.highInstitution = highInstitution;
		this.highSpecialization = highSpecialization;
		this.highCourseType = highCourseType;
		this.highBoard = highBoard;
		this.highTotalMarks = highTotalMarks;
		this.highObtainMarks = highObtainMarks;
		this.highPercentage = highPercentage;
		this.highCertificate = highCertificate;
		this.highGrade = highGrade;
		this.interGrade = interGrade;
		this.interYear = interYear;
		this.interInstitution = interInstitution;
		this.interSpecialization = interSpecialization;
		this.interCourseType = interCourseType;
		this.interBoard = interBoard;
		this.interTotalMarks = interTotalMarks;
		this.interObtainMarks = interObtainMarks;
		this.interPercentage = interPercentage;
		this.interCertificate = interCertificate;
		this.englishTyping = englishTyping;
		this.hindiTyping = hindiTyping;
		this.additionalCertificate = additionalCertificate;
		this.englishStenography = englishStenography;
		this.hindiStenography = hindiStenography;
		this.firstaidCertificate = firstaidCertificate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getHighYear() {
		return highYear;
	}

	public void setHighYear(int highYear) {
		this.highYear = highYear;
	}

	public String getHighInstitution() {
		return highInstitution;
	}

	public void setHighInstitution(String highInstitution) {
		this.highInstitution = highInstitution;
	}

	public String getHighSpecialization() {
		return highSpecialization;
	}

	public void setHighSpecialization(String highSpecialization) {
		this.highSpecialization = highSpecialization;
	}

	public String getHighCourseType() {
		return highCourseType;
	}

	public void setHighCourseType(String highCourseType) {
		this.highCourseType = highCourseType;
	}

	public String getHighBoard() {
		return highBoard;
	}

	public void setHighBoard(String highBoard) {
		this.highBoard = highBoard;
	}

	public int getHighTotalMarks() {
		return highTotalMarks;
	}

	public void setHighTotalMarks(int highTotalMarks) {
		this.highTotalMarks = highTotalMarks;
	}

	public int getHighObtainMarks() {
		return highObtainMarks;
	}

	public void setHighObtainMarks(int highObtainMarks) {
		this.highObtainMarks = highObtainMarks;
	}

	public float getHighPercentage() {
		return highPercentage;
	}

	public void setHighPercentage(float highPercentage) {
		this.highPercentage = highPercentage;
	}

	public String getHighCertificate() {
		return highCertificate;
	}

	public void setHighCertificate(String highCertificate) {
		this.highCertificate = highCertificate;
	}

	public String getHighGrade() {
		return highGrade;
	}

	public void setHighGrade(String highGrade) {
		this.highGrade = highGrade;
	}

	public String getInterGrade() {
		return interGrade;
	}

	public void setInterGrade(String interGrade) {
		this.interGrade = interGrade;
	}

	public int getInterYear() {
		return interYear;
	}

	public void setInterYear(int interYear) {
		this.interYear = interYear;
	}

	public String getInterInstitution() {
		return interInstitution;
	}

	public void setInterInstitution(String interInstitution) {
		this.interInstitution = interInstitution;
	}

	public String getInterSpecialization() {
		return interSpecialization;
	}

	public void setInterSpecialization(String interSpecialization) {
		this.interSpecialization = interSpecialization;
	}

	public String getInterCourseType() {
		return interCourseType;
	}

	public void setInterCourseType(String interCourseType) {
		this.interCourseType = interCourseType;
	}

	public String getInterBoard() {
		return interBoard;
	}

	public void setInterBoard(String interBoard) {
		this.interBoard = interBoard;
	}

	public int getInterTotalMarks() {
		return interTotalMarks;
	}

	public void setInterTotalMarks(int interTotalMarks) {
		this.interTotalMarks = interTotalMarks;
	}

	public int getInterObtainMarks() {
		return interObtainMarks;
	}

	public void setInterObtainMarks(int interObtainMarks) {
		this.interObtainMarks = interObtainMarks;
	}

	public float getInterPercentage() {
		return interPercentage;
	}

	public void setInterPercentage(float interPercentage) {
		this.interPercentage = interPercentage;
	}

	public String getInterCertificate() {
		return interCertificate;
	}

	public void setInterCertificate(String interCertificate) {
		this.interCertificate = interCertificate;
	}

	public int getEnglishTyping() {
		return englishTyping;
	}

	public void setEnglishTyping(int englishTyping) {
		this.englishTyping = englishTyping;
	}

	public int getHindiTyping() {
		return hindiTyping;
	}

	public void setHindiTyping(int hindiTyping) {
		this.hindiTyping = hindiTyping;
	}

	public String getAdditionalCertificate() {
		return additionalCertificate;
	}

	public void setAdditionalCertificate(String additionalCertificate) {
		this.additionalCertificate = additionalCertificate;
	}

	public int getEnglishStenography() {
		return englishStenography;
	}

	public void setEnglishStenography(int englishStenography) {
		this.englishStenography = englishStenography;
	}

	public int getHindiStenography() {
		return hindiStenography;
	}

	public void setHindiStenography(int hindiStenography) {
		this.hindiStenography = hindiStenography;
	}

	public String getFirstaidCertificate() {
		return firstaidCertificate;
	}

	public void setFirstaidCertificate(String firstaidCertificate) {
		this.firstaidCertificate = firstaidCertificate;
	}

	@Override
	public String toString() {
		return "EssentialQualification [id=" + id + ", highYear=" + highYear + ", highInstitution=" + highInstitution
				+ ", highSpecialization=" + highSpecialization + ", highCourseType=" + highCourseType + ", highBoard="
				+ highBoard + ", highTotalMarks=" + highTotalMarks + ", highObtainMarks=" + highObtainMarks
				+ ", highPercentage=" + highPercentage + ", highCertificate=" + highCertificate + ", highGrade="
				+ highGrade + ", interGrade=" + interGrade + ", interYear=" + interYear + ", interInstitution="
				+ interInstitution + ", interSpecialization=" + interSpecialization + ", interCourseType="
				+ interCourseType + ", interBoard=" + interBoard + ", interTotalMarks=" + interTotalMarks
				+ ", interObtainMarks=" + interObtainMarks + ", interPercentage=" + interPercentage
				+ ", interCertificate=" + interCertificate + ", englishTyping=" + englishTyping + ", hindiTyping="
				+ hindiTyping + ", additionalCertificate=" + additionalCertificate + ", englishStenography="
				+ englishStenography + ", hindiStenography=" + hindiStenography + ", firstaidCertificate="
				+ firstaidCertificate + "]";
	}

	
		
}
