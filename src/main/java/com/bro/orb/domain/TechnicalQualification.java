package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bro_candidate_technical_qualification")
public class TechnicalQualification extends AbstractAuditingEntity implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	
	@Column(name = "diploma_passing_year")
	private int diplomaYear;
	
	
	@Column(name = "diploma_institution")
	private String diplomaInstitution;
	
	
	@Column(name = "diploma_specialization")
	private String diplomaSpecialization;
	
	
	@Column(name = "diploma_branch")
	private String diplomaBranch;
	
	
	@Column(name = "diploma_university")
	private String diplomaUniversity;
	
	
	@Column(name = "diploma_total_marks")
	private int diplomaTotalMarks;
	
	
	@Column(name = "diploma_marks_obtain")
	private int diplomaObtainMarks;
	
	
	@Column(name = "diploma_percentage")
	private float diplomaPercentage;
	
	
	@Column(name = "diploma_course_type")
	private String diplomaCourseType;
	
	
	@Column(name = "diploma_certificate")
	private String diplomaCertificate;
	
	
	@Column(name = "degree_passing_year")
	private int degreeYear;
	
	
	@Column(name = "degree_institution")
	private String degreeInstitution;
	
	
	@Column(name = "degree_specialization")
	private String degreeSpecialization;
	
	
	@Column(name = "degree_course_type")
	private String degreeCourseType;
	
	
	@Column(name = "degree_branch")
	private String degreeBranch;
	
	@Column(name = "degree_university")
	private String degreeUniversity;
	
	
	@Column(name = "degree_total_marks")
	private int degreeTotalMarks;
	
	
	@Column(name = "degree_marks_obtain")
	private int degreeMarksObtain;
	
	
	@Column(name = "degree_percentage")
	private float degreePercentage;
	
	
	@Column(name = "degree_certificate")
	private String degreeCertificate;
	
	@Column(name = "iti_passing_year")
	private int itiYear;
	
	
	@Column(name = "iti_institution")
	private String itiInstitution;
	
	
	@Column(name = "iti_specialization")
	private String itiSpecialization;
	
	
	@Column(name = "iti_course_type")
	private String itiCourseType;
	
	
	@Column(name = "iti_branch")
	private String itiBranch;
	
	@Column(name = "iti_university")
	private String itiUniversity;
	
	
	@Column(name = "iti_total_marks")
	private int itiTotalMarks;
	
	
	@Column(name = "iti_marks_obtain")
	private int itiMarksObtain;
	
	
	@Column(name = "iti_percentage")
	private float itiPercentage;
	
	
	@Column(name = "iti_certificate")
	private String itiCertificate;
	
	
	public TechnicalQualification() {
		super();
	}


	public TechnicalQualification(long id, int diplomaYear, String diplomaInstitution, String diplomaSpecialization,
			String diplomaBranch, String diplomaUniversity, int diplomaTotalMarks, int diplomaObtainMarks,
			float diplomaPercentage, String diplomaCourseType, String diplomaCertificate, int degreeYear,
			String degreeInstitution, String degreeSpecialization, String degreeCourseType, String degreeBranch,
			String degreeUniversity, int degreeTotalMarks, int degreeMarksObtain, float degreePercentage,
			String degreeCertificate, int itiYear, String itiInstitution, String itiSpecialization,
			String itiCourseType, String itiBranch, String itiUniversity, int itiTotalMarks, int itiMarksObtain,
			float itiPercentage, String itiCertificate) {
		super();
		this.id = id;
		this.diplomaYear = diplomaYear;
		this.diplomaInstitution = diplomaInstitution;
		this.diplomaSpecialization = diplomaSpecialization;
		this.diplomaBranch = diplomaBranch;
		this.diplomaUniversity = diplomaUniversity;
		this.diplomaTotalMarks = diplomaTotalMarks;
		this.diplomaObtainMarks = diplomaObtainMarks;
		this.diplomaPercentage = diplomaPercentage;
		this.diplomaCourseType = diplomaCourseType;
		this.diplomaCertificate = diplomaCertificate;
		this.degreeYear = degreeYear;
		this.degreeInstitution = degreeInstitution;
		this.degreeSpecialization = degreeSpecialization;
		this.degreeCourseType = degreeCourseType;
		this.degreeBranch = degreeBranch;
		this.degreeUniversity = degreeUniversity;
		this.degreeTotalMarks = degreeTotalMarks;
		this.degreeMarksObtain = degreeMarksObtain;
		this.degreePercentage = degreePercentage;
		this.degreeCertificate = degreeCertificate;
		this.itiYear = itiYear;
		this.itiInstitution = itiInstitution;
		this.itiSpecialization = itiSpecialization;
		this.itiCourseType = itiCourseType;
		this.itiBranch = itiBranch;
		this.itiUniversity = itiUniversity;
		this.itiTotalMarks = itiTotalMarks;
		this.itiMarksObtain = itiMarksObtain;
		this.itiPercentage = itiPercentage;
		this.itiCertificate = itiCertificate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getDiplomaYear() {
		return diplomaYear;
	}


	public void setDiplomaYear(int diplomaYear) {
		this.diplomaYear = diplomaYear;
	}


	public String getDiplomaInstitution() {
		return diplomaInstitution;
	}


	public void setDiplomaInstitution(String diplomaInstitution) {
		this.diplomaInstitution = diplomaInstitution;
	}


	public String getDiplomaSpecialization() {
		return diplomaSpecialization;
	}


	public void setDiplomaSpecialization(String diplomaSpecialization) {
		this.diplomaSpecialization = diplomaSpecialization;
	}


	public String getDiplomaBranch() {
		return diplomaBranch;
	}


	public void setDiplomaBranch(String diplomaBranch) {
		this.diplomaBranch = diplomaBranch;
	}


	public String getDiplomaUniversity() {
		return diplomaUniversity;
	}


	public void setDiplomaUniversity(String diplomaUniversity) {
		this.diplomaUniversity = diplomaUniversity;
	}


	public int getDiplomaTotalMarks() {
		return diplomaTotalMarks;
	}


	public void setDiplomaTotalMarks(int diplomaTotalMarks) {
		this.diplomaTotalMarks = diplomaTotalMarks;
	}


	public int getDiplomaObtainMarks() {
		return diplomaObtainMarks;
	}


	public void setDiplomaObtainMarks(int diplomaObtainMarks) {
		this.diplomaObtainMarks = diplomaObtainMarks;
	}


	public float getDiplomaPercentage() {
		return diplomaPercentage;
	}


	public void setDiplomaPercentage(float diplomaPercentage) {
		this.diplomaPercentage = diplomaPercentage;
	}


	public String getDiplomaCourseType() {
		return diplomaCourseType;
	}


	public void setDiplomaCourseType(String diplomaCourseType) {
		this.diplomaCourseType = diplomaCourseType;
	}


	public String getDiplomaCertificate() {
		return diplomaCertificate;
	}


	public void setDiplomaCertificate(String diplomaCertificate) {
		this.diplomaCertificate = diplomaCertificate;
	}


	public int getDegreeYear() {
		return degreeYear;
	}


	public void setDegreeYear(int degreeYear) {
		this.degreeYear = degreeYear;
	}


	public String getDegreeInstitution() {
		return degreeInstitution;
	}


	public void setDegreeInstitution(String degreeInstitution) {
		this.degreeInstitution = degreeInstitution;
	}


	public String getDegreeSpecialization() {
		return degreeSpecialization;
	}


	public void setDegreeSpecialization(String degreeSpecialization) {
		this.degreeSpecialization = degreeSpecialization;
	}


	public String getDegreeCourseType() {
		return degreeCourseType;
	}


	public void setDegreeCourseType(String degreeCourseType) {
		this.degreeCourseType = degreeCourseType;
	}


	public String getDegreeBranch() {
		return degreeBranch;
	}


	public void setDegreeBranch(String degreeBranch) {
		this.degreeBranch = degreeBranch;
	}


	public String getDegreeUniversity() {
		return degreeUniversity;
	}


	public void setDegreeUniversity(String degreeUniversity) {
		this.degreeUniversity = degreeUniversity;
	}


	public int getDegreeTotalMarks() {
		return degreeTotalMarks;
	}


	public void setDegreeTotalMarks(int degreeTotalMarks) {
		this.degreeTotalMarks = degreeTotalMarks;
	}


	public int getDegreeMarksObtain() {
		return degreeMarksObtain;
	}


	public void setDegreeMarksObtain(int degreeMarksObtain) {
		this.degreeMarksObtain = degreeMarksObtain;
	}


	public float getDegreePercentage() {
		return degreePercentage;
	}


	public void setDegreePercentage(float degreePercentage) {
		this.degreePercentage = degreePercentage;
	}


	public String getDegreeCertificate() {
		return degreeCertificate;
	}


	public void setDegreeCertificate(String degreeCertificate) {
		this.degreeCertificate = degreeCertificate;
	}


	public int getItiYear() {
		return itiYear;
	}


	public void setItiYear(int itiYear) {
		this.itiYear = itiYear;
	}


	public String getItiInstitution() {
		return itiInstitution;
	}


	public void setItiInstitution(String itiInstitution) {
		this.itiInstitution = itiInstitution;
	}


	public String getItiSpecialization() {
		return itiSpecialization;
	}


	public void setItiSpecialization(String itiSpecialization) {
		this.itiSpecialization = itiSpecialization;
	}


	public String getItiCourseType() {
		return itiCourseType;
	}


	public void setItiCourseType(String itiCourseType) {
		this.itiCourseType = itiCourseType;
	}


	public String getItiBranch() {
		return itiBranch;
	}


	public void setItiBranch(String itiBranch) {
		this.itiBranch = itiBranch;
	}


	public String getItiUniversity() {
		return itiUniversity;
	}


	public void setItiUniversity(String itiUniversity) {
		this.itiUniversity = itiUniversity;
	}


	public int getItiTotalMarks() {
		return itiTotalMarks;
	}


	public void setItiTotalMarks(int itiTotalMarks) {
		this.itiTotalMarks = itiTotalMarks;
	}


	public int getItiMarksObtain() {
		return itiMarksObtain;
	}


	public void setItiMarksObtain(int itiMarksObtain) {
		this.itiMarksObtain = itiMarksObtain;
	}


	public float getItiPercentage() {
		return itiPercentage;
	}


	public void setItiPercentage(float itiPercentage) {
		this.itiPercentage = itiPercentage;
	}


	public String getItiCertificate() {
		return itiCertificate;
	}


	public void setItiCertificate(String itiCertificate) {
		this.itiCertificate = itiCertificate;
	}


	@Override
	public String toString() {
		return "TechnicalQualification [id=" + id + ", diplomaYear=" + diplomaYear + ", diplomaInstitution="
				+ diplomaInstitution + ", diplomaSpecialization=" + diplomaSpecialization + ", diplomaBranch="
				+ diplomaBranch + ", diplomaUniversity=" + diplomaUniversity + ", diplomaTotalMarks="
				+ diplomaTotalMarks + ", diplomaObtainMarks=" + diplomaObtainMarks + ", diplomaPercentage="
				+ diplomaPercentage + ", diplomaCourseType=" + diplomaCourseType + ", diplomaCertificate="
				+ diplomaCertificate + ", degreeYear=" + degreeYear + ", degreeInstitution=" + degreeInstitution
				+ ", degreeSpecialization=" + degreeSpecialization + ", degreeCourseType=" + degreeCourseType
				+ ", degreeBranch=" + degreeBranch + ", degreeUniversity=" + degreeUniversity + ", degreeTotalMarks="
				+ degreeTotalMarks + ", degreeMarksObtain=" + degreeMarksObtain + ", degreePercentage="
				+ degreePercentage + ", degreeCertificate=" + degreeCertificate + ", itiYear=" + itiYear
				+ ", itiInstitution=" + itiInstitution + ", itiSpecialization=" + itiSpecialization + ", itiCourseType="
				+ itiCourseType + ", itiBranch=" + itiBranch + ", itiUniversity=" + itiUniversity + ", itiTotalMarks="
				+ itiTotalMarks + ", itiMarksObtain=" + itiMarksObtain + ", itiPercentage=" + itiPercentage
				+ ", itiCertificate=" + itiCertificate + "]";
	}

	}
