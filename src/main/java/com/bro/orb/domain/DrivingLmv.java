package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_driving_lmv")
public class DrivingLmv implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "licence_number_lmv")
	private boolean licenceNumberLmvView;

	@Column(name = "issue_date_lmv")
	private boolean issueDateLmvView;

	@Column(name = "valid_upto_lmv")
	private boolean validUptoLmvView;

	@Column(name = "issuing_authority_lmv")
	private boolean issuingAuthorityLmvView;

	@Column(name = "lmv_experience")
	private boolean lmvExperienceView;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public DrivingLmv() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLicenceNumberLmvView() {
		return licenceNumberLmvView;
	}

	public void setLicenceNumberLmvView(boolean licenceNumberLmvView) {
		this.licenceNumberLmvView = licenceNumberLmvView;
	}

	public boolean isIssueDateLmvView() {
		return issueDateLmvView;
	}

	public void setIssueDateLmvView(boolean issueDateLmvView) {
		this.issueDateLmvView = issueDateLmvView;
	}

	public boolean isValidUptoLmvView() {
		return validUptoLmvView;
	}

	public void setValidUptoLmvView(boolean validUptoLmvView) {
		this.validUptoLmvView = validUptoLmvView;
	}

	public boolean isIssuingAuthorityLmvView() {
		return issuingAuthorityLmvView;
	}

	public void setIssuingAuthorityLmvView(boolean issuingAuthorityLmvView) {
		this.issuingAuthorityLmvView = issuingAuthorityLmvView;
	}

	public boolean isLmvExperienceView() {
		return lmvExperienceView;
	}

	public void setLmvExperienceView(boolean lmvExperienceView) {
		this.lmvExperienceView = lmvExperienceView;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	public DrivingLmv(int id, boolean licenceNumberLmvView, boolean issueDateLmvView, boolean validUptoLmvView,
			boolean issuingAuthorityLmvView, boolean lmvExperienceView, RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.licenceNumberLmvView = licenceNumberLmvView;
		this.issueDateLmvView = issueDateLmvView;
		this.validUptoLmvView = validUptoLmvView;
		this.issuingAuthorityLmvView = issuingAuthorityLmvView;
		this.lmvExperienceView = lmvExperienceView;
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "DrivingLmv [id=" + id + ", licenceNumberLmvView=" + licenceNumberLmvView + ", issueDateLmvView="
				+ issueDateLmvView + ", validUptoLmvView=" + validUptoLmvView + ", issuingAuthorityLmvView="
				+ issuingAuthorityLmvView + ", lmvExperienceView=" + lmvExperienceView + ", registrationViewBean="
				+ registrationViewBean + "]";
	}

	
}
