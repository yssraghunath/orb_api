package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bro_driving_hgmv")
public class DrivingHgmv implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "licence_number_hgmv")
	private boolean licenceNumberHgmvView;

	@Column(name = "issue_date_hgmv")
	private boolean issueDateHgmvView;

	@Column(name = "valid_upto_hgmv")
	private boolean validUptoHgmvView;

	@Column(name = "issuing_authority_hgmv")
	private boolean issuingAuthorityHgmvView;

	@Column(name = "hgmv_experience")
	private boolean hgmvExperienceView;

	public DrivingHgmv() {
		super();
	}

	public DrivingHgmv(int id, boolean licenceNumberHgmvView, boolean issueDateHgmvView, boolean validUptoHgmvView,
			boolean issuingAuthorityHgmvView, boolean hgmvExperienceView) {
		super();
		this.id = id;
		this.licenceNumberHgmvView = licenceNumberHgmvView;
		this.issueDateHgmvView = issueDateHgmvView;
		this.validUptoHgmvView = validUptoHgmvView;
		this.issuingAuthorityHgmvView = issuingAuthorityHgmvView;
		this.hgmvExperienceView = hgmvExperienceView;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLicenceNumberHgmvView() {
		return licenceNumberHgmvView;
	}

	public void setLicenceNumberHgmvView(boolean licenceNumberHgmvView) {
		this.licenceNumberHgmvView = licenceNumberHgmvView;
	}

	public boolean isIssueDateHgmvView() {
		return issueDateHgmvView;
	}

	public void setIssueDateHgmvView(boolean issueDateHgmvView) {
		this.issueDateHgmvView = issueDateHgmvView;
	}

	public boolean isValidUptoHgmvView() {
		return validUptoHgmvView;
	}

	public void setValidUptoHgmvView(boolean validUptoHgmvView) {
		this.validUptoHgmvView = validUptoHgmvView;
	}

	public boolean isIssuingAuthorityHgmvView() {
		return issuingAuthorityHgmvView;
	}

	public void setIssuingAuthorityHgmvView(boolean issuingAuthorityHgmvView) {
		this.issuingAuthorityHgmvView = issuingAuthorityHgmvView;
	}

	public boolean isHgmvExperienceView() {
		return hgmvExperienceView;
	}

	public void setHgmvExperienceView(boolean hgmvExperienceView) {
		this.hgmvExperienceView = hgmvExperienceView;
	}

	@Override
	public String toString() {
		return "DrivingHgmv [id=" + id + ", licenceNumberHgmvView=" + licenceNumberHgmvView + ", issueDateHgmvView="
				+ issueDateHgmvView + ", validUptoHgmvView=" + validUptoHgmvView + ", issuingAuthorityHgmvView="
				+ issuingAuthorityHgmvView + ", hgmvExperienceView=" + hgmvExperienceView + "]";
	}

}
