package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="bro_iti")
public class Iti implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "iti_year")
	private boolean itiYearView;
	
	@Column(name = "iti_institution")
	private boolean itiInstitutionView;
	
	@Column(name = "iti_specialization")
	private boolean itiSpecializationView;
	
	@Column(name = "iti_course_type")
	private boolean itiCourseTypeView;
	
	@Column(name = "iti_total_marks")
	private boolean itiTotalMarksView;
	
	@Column(name = "iti_obtain_marks")
	private boolean itiObtainarksView;
	
	@Column(name = "iti_percentage")
	private boolean itiPercentageView;
	
	@Column(name = "iti_university")
	private boolean itiUniversityView;
	
	@Column(name = "iti_certificate")
	private boolean itiCertificateView;
	
	@Column(name = "iti_min_percent")
	private int itiMinPercent;
	
	@Column(name = "iti_branch")
	private boolean itiBranchView;
	public Iti() {
		super();
	}
	public Iti(int id, boolean itiYearView, boolean itiInstitutionView, boolean itiSpecializationView,
			boolean itiCourseTypeView, boolean itiTotalMarksView, boolean itiObtainarksView, boolean itiPercentageView,
			boolean itiUniversityView, boolean itiCertificateView, int itiMinPercent, boolean itiBranchView) {
		super();
		this.id = id;
		this.itiYearView = itiYearView;
		this.itiInstitutionView = itiInstitutionView;
		this.itiSpecializationView = itiSpecializationView;
		this.itiCourseTypeView = itiCourseTypeView;
		this.itiTotalMarksView = itiTotalMarksView;
		this.itiObtainarksView = itiObtainarksView;
		this.itiPercentageView = itiPercentageView;
		this.itiUniversityView = itiUniversityView;
		this.itiCertificateView = itiCertificateView;
		this.itiMinPercent = itiMinPercent;
		this.itiBranchView = itiBranchView;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isItiYearView() {
		return itiYearView;
	}
	public void setItiYearView(boolean itiYearView) {
		this.itiYearView = itiYearView;
	}
	public boolean isItiInstitutionView() {
		return itiInstitutionView;
	}
	public void setItiInstitutionView(boolean itiInstitutionView) {
		this.itiInstitutionView = itiInstitutionView;
	}
	public boolean isItiSpecializationView() {
		return itiSpecializationView;
	}
	public void setItiSpecializationView(boolean itiSpecializationView) {
		this.itiSpecializationView = itiSpecializationView;
	}
	public boolean isItiCourseTypeView() {
		return itiCourseTypeView;
	}
	public void setItiCourseTypeView(boolean itiCourseTypeView) {
		this.itiCourseTypeView = itiCourseTypeView;
	}
	public boolean isItiTotalMarksView() {
		return itiTotalMarksView;
	}
	public void setItiTotalMarksView(boolean itiTotalMarksView) {
		this.itiTotalMarksView = itiTotalMarksView;
	}
	public boolean isItiObtainarksView() {
		return itiObtainarksView;
	}
	public void setItiObtainarksView(boolean itiObtainarksView) {
		this.itiObtainarksView = itiObtainarksView;
	}
	public boolean isItiPercentageView() {
		return itiPercentageView;
	}
	public void setItiPercentageView(boolean itiPercentageView) {
		this.itiPercentageView = itiPercentageView;
	}
	public boolean isItiUniversityView() {
		return itiUniversityView;
	}
	public void setItiUniversityView(boolean itiUniversityView) {
		this.itiUniversityView = itiUniversityView;
	}
	public boolean isItiCertificateView() {
		return itiCertificateView;
	}
	public void setItiCertificateView(boolean itiCertificateView) {
		this.itiCertificateView = itiCertificateView;
	}
	public int getItiMinPercent() {
		return itiMinPercent;
	}
	public void setItiMinPercent(int itiMinPercent) {
		this.itiMinPercent = itiMinPercent;
	}
	public boolean isItiBranchView() {
		return itiBranchView;
	}
	public void setItiBranchView(boolean itiBranchView) {
		this.itiBranchView = itiBranchView;
	}
	@Override
	public String toString() {
		return "Iti [id=" + id + ", itiYearView=" + itiYearView + ", itiInstitutionView=" + itiInstitutionView
				+ ", itiSpecializationView=" + itiSpecializationView + ", itiCourseTypeView=" + itiCourseTypeView
				+ ", itiTotalMarksView=" + itiTotalMarksView + ", itiObtainarksView=" + itiObtainarksView
				+ ", itiPercentageView=" + itiPercentageView + ", itiUniversityView=" + itiUniversityView
				+ ", itiCertificateView=" + itiCertificateView + ", itiMinPercent=" + itiMinPercent + ", itiBranchView="
				+ itiBranchView + "]";
	}
}
