package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_degree")
public class Degree implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "degree_year")
	private boolean degreeYearView;
	
	@Column(name = "degree_institution")
	private boolean degreeInstitutionView;
	
	@Column(name = "degree_specialization")
	private boolean degreeSpecializationView;
	
	@Column(name = "degree_course_type")
	private boolean degreeCourseTypeView;
	
	@Column(name = "degree_total_marks")
	private boolean degreeTotalMarksView;
	
	@Column(name = "degree_obtain_marks")
	private boolean degreeObtainarksView;
	
	@Column(name = "degree_percentage")
	private boolean degreePercentageView;
	
	@Column(name = "degree_university")
	private boolean degreeUniversityView;
	
	@Column(name = "degree_certificate")
	private boolean degreeCertificateView;
	
	@Column(name = "degree_branch")
	private boolean degreeBranchView;
	
	@Column(name = "degree_min_percent")
	private int degreeMinPercent;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public Degree() {
		super();
	}

	public Degree(int id, boolean degreeYearView, boolean degreeInstitutionView, boolean degreeSpecializationView,
			boolean degreeCourseTypeView, boolean degreeTotalMarksView, boolean degreeObtainarksView,
			boolean degreePercentageView, boolean degreeUniversityView, boolean degreeCertificateView,
			boolean degreeBranchView, int degreeMinPercent, RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.degreeYearView = degreeYearView;
		this.degreeInstitutionView = degreeInstitutionView;
		this.degreeSpecializationView = degreeSpecializationView;
		this.degreeCourseTypeView = degreeCourseTypeView;
		this.degreeTotalMarksView = degreeTotalMarksView;
		this.degreeObtainarksView = degreeObtainarksView;
		this.degreePercentageView = degreePercentageView;
		this.degreeUniversityView = degreeUniversityView;
		this.degreeCertificateView = degreeCertificateView;
		this.degreeBranchView = degreeBranchView;
		this.degreeMinPercent = degreeMinPercent;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDegreeYearView() {
		return degreeYearView;
	}

	public void setDegreeYearView(boolean degreeYearView) {
		this.degreeYearView = degreeYearView;
	}

	public boolean isDegreeInstitutionView() {
		return degreeInstitutionView;
	}

	public void setDegreeInstitutionView(boolean degreeInstitutionView) {
		this.degreeInstitutionView = degreeInstitutionView;
	}

	public boolean isDegreeSpecializationView() {
		return degreeSpecializationView;
	}

	public void setDegreeSpecializationView(boolean degreeSpecializationView) {
		this.degreeSpecializationView = degreeSpecializationView;
	}

	public boolean isDegreeCourseTypeView() {
		return degreeCourseTypeView;
	}

	public void setDegreeCourseTypeView(boolean degreeCourseTypeView) {
		this.degreeCourseTypeView = degreeCourseTypeView;
	}

	public boolean isDegreeTotalMarksView() {
		return degreeTotalMarksView;
	}

	public void setDegreeTotalMarksView(boolean degreeTotalMarksView) {
		this.degreeTotalMarksView = degreeTotalMarksView;
	}

	public boolean isDegreeObtainarksView() {
		return degreeObtainarksView;
	}

	public void setDegreeObtainarksView(boolean degreeObtainarksView) {
		this.degreeObtainarksView = degreeObtainarksView;
	}

	public boolean isDegreePercentageView() {
		return degreePercentageView;
	}

	public void setDegreePercentageView(boolean degreePercentageView) {
		this.degreePercentageView = degreePercentageView;
	}

	public boolean isDegreeUniversityView() {
		return degreeUniversityView;
	}

	public void setDegreeUniversityView(boolean degreeUniversityView) {
		this.degreeUniversityView = degreeUniversityView;
	}

	public boolean isDegreeCertificateView() {
		return degreeCertificateView;
	}

	public void setDegreeCertificateView(boolean degreeCertificateView) {
		this.degreeCertificateView = degreeCertificateView;
	}

	public boolean isDegreeBranchView() {
		return degreeBranchView;
	}

	public void setDegreeBranchView(boolean degreeBranchView) {
		this.degreeBranchView = degreeBranchView;
	}

	public int getDegreeMinPercent() {
		return degreeMinPercent;
	}

	public void setDegreeMinPercent(int degreeMinPercent) {
		this.degreeMinPercent = degreeMinPercent;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "Degree [id=" + id + ", degreeYearView=" + degreeYearView + ", degreeInstitutionView="
				+ degreeInstitutionView + ", degreeSpecializationView=" + degreeSpecializationView
				+ ", degreeCourseTypeView=" + degreeCourseTypeView + ", degreeTotalMarksView=" + degreeTotalMarksView
				+ ", degreeObtainarksView=" + degreeObtainarksView + ", degreePercentageView=" + degreePercentageView
				+ ", degreeUniversityView=" + degreeUniversityView + ", degreeCertificateView=" + degreeCertificateView
				+ ", degreeBranchView=" + degreeBranchView + ", degreeMinPercent=" + degreeMinPercent
				+ ", registrationViewBean=" + registrationViewBean + "]";
	}

	
}
