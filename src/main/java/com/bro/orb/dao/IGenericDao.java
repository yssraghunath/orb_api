package com.bro.orb.dao;

import java.util.List;

public interface IGenericDao<T> {

	T findOne(T entity, long id);

	T findOneByCondition(T entity, String condition);
	
	T findOneByCondition(T entity, String condition,int index);

	List<T> findAll(T entity);

	List<T> findAllByCondition(T entity, String condition); 

	void save(T entity);

	T update(T entity);

	void delete(T entity);

	boolean exists(T entity, long entityId);
}