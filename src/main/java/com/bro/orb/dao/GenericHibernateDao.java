package com.bro.orb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class GenericHibernateDao<T> implements IGenericDao<T> {

	@PersistenceContext
	EntityManager entityManager;

	public T findOne(T entity, long id) {
		return (T) entityManager.find(entity.getClass(), id);
	}

	public List<T> findAll(T entity) {
		return entityManager.createQuery("from " + entity.getClass().getName())
				.getResultList();
	}

	public List<T> findAllByCondition(T entity, String condition) {
		if (condition != null && condition != "") {
			return entityManager.createQuery(
					"from " + entity.getClass().getName()+" obj "+ condition).getResultList();
		} else {
			return entityManager.createQuery(
					"from " + entity.getClass().getName()).getResultList();
		}

	}
	 
	public T findOneByCondition(T entity, String condition) {
		if (condition != null && condition != "") {
			return (T) entityManager.createQuery(
					"from " + entity.getClass().getName() + " where "
							+ condition).getSingleResult();
		} else {
			return (T) entityManager.createQuery(
					"from " + entity.getClass().getName()).getSingleResult();
		}

	}
	
	
	public T findOneByCondition(T entity, String condition,int index) {
		if (condition != null && condition != "") {
			List<T>  list = entityManager.createQuery("from " + entity.getClass().getName()+ condition).getResultList();
			if(list.size() > 0 && index < list.size()){
				return list.get(index);
			} else {
				return null;
			}
		} else {
			return (T) entityManager.createQuery("from " + entity.getClass().getName()).getSingleResult();
		}

	}

	public void save(T entity) {
		entityManager.persist(entity);
	}

	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entityManager.contains(entity) ? entity
				: entityManager.merge(entity));
	}

	public boolean exists(T entity, long entityId) {
		return findOne(entity, entityId) == null ? false : true;
	}

}