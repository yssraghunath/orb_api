package com.bro.orb.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bro.orb.domain.Board;
import com.bro.orb.domain.Category;
import com.bro.orb.domain.City;
import com.bro.orb.domain.Course;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.PersonalDetails;
import com.bro.orb.domain.State;
import com.bro.orb.domain.University;
import com.bro.orb.domain.User;
import com.bro.orb.domain.Years;
import com.bro.orb.repository.BoardRepository;
import com.bro.orb.repository.CityRepository;
import com.bro.orb.repository.CourseRepository;
import com.bro.orb.repository.JobDetailsRepository;
import com.bro.orb.repository.StateRepository;
import com.bro.orb.repository.UniversityRepository;
import com.bro.orb.repository.UserRepository;

@Service
@Transactional
public class RestCommonService {

	private final Logger log = LoggerFactory.getLogger(RestCommonService.class);

	@Autowired
	private JobDetailsRepository jobRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private BoardRepository boardRepository;

	@Autowired
	private UniversityRepository universityRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private UserRepository userRepository;

	public List<State> findState() {
		return stateRepository.findAll();
	}

	public List<City> findById(int id) {
		return cityRepository.findById(id);
	}

	public List<Board> findBoard() {
		return boardRepository.findAll();
	}

	public List<Course> findCourse(String type) {
		return courseRepository.findByOne(type);
	}
	public List<Course> findCourseType() {
		return courseRepository.findAll();
	}

	public List<University> findUniversity() {

		return universityRepository.findAll();
	}

	public Optional<User> checkForEmail(String email) {
		log.debug("cheking mail for :" + email);
		return userRepository.findOneByEmail(email);
	}

	public List<City> findByName(String name) {
		return cityRepository.findByName(name);
	}
	
	
}
