package com.bro.orb.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bro.orb.domain.AdminRegister;
import com.bro.orb.domain.Authority;
import com.bro.orb.domain.CenterAddress;
import com.bro.orb.domain.Degree;
import com.bro.orb.domain.Diploma;
import com.bro.orb.domain.DrivingHgmv;
import com.bro.orb.domain.DrivingLmv;
import com.bro.orb.domain.DrivingRoadRoller;
import com.bro.orb.domain.ExaminationCenter;
import com.bro.orb.domain.ExaminationCenterAllocation;
import com.bro.orb.domain.Graduation;
import com.bro.orb.domain.Intermediate;
import com.bro.orb.domain.Iti;
import com.bro.orb.domain.JobAmountDetails;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.JobFormFields;
import com.bro.orb.domain.JobsDescription;
import com.bro.orb.domain.JobsFormDescription;
import com.bro.orb.domain.LoaderExcavator;
import com.bro.orb.domain.OrganisationExperience;
import com.bro.orb.domain.PostGraduation;
import com.bro.orb.domain.RecruitmentZone;
import com.bro.orb.domain.RegistrationViewBean;
import com.bro.orb.domain.ShortListADrivingLicense;
import com.bro.orb.domain.ShortListAEssentialQualification;
import com.bro.orb.domain.ShortListAGraduationQualification;
import com.bro.orb.domain.ShortListAJobDetails;
import com.bro.orb.domain.ShortListAOrganisationExperience;
import com.bro.orb.domain.ShortListAPersonalDetails;
import com.bro.orb.domain.ShortListATechnicalQualification;
import com.bro.orb.domain.ShortListAUploadCertificate;
import com.bro.orb.domain.User;
import com.bro.orb.domain.Zone;
import com.bro.orb.repository.AuthorityRepository;
import com.bro.orb.repository.ExamCenterRepository;
import com.bro.orb.repository.JobAmountRepository;
import com.bro.orb.repository.JobDescriptionRepository;
import com.bro.orb.repository.JobDetailsRepository;
import com.bro.orb.repository.RegistrationViewRepository;
import com.bro.orb.repository.ShortListAJobDetailsRepository;
import com.bro.orb.repository.ShortListAPersonalRepository;
import com.bro.orb.repository.ZoneRepository;
import com.bro.orb.security.SecurityUtils;
import com.bro.orb.service.util.RandomUtil;
import com.mysql.jdbc.log.Log;

@Service
@Transactional
public class AdminService {

	@Value("${filepath.uploadpath}")
	//private String UPLOADED_FOLDER;
	private static String UPLOADED_FOLDER ="E:/ORP/src/assets/images/";

	private final Logger log = LoggerFactory.getLogger(AdminService.class);

	@Autowired
	private JobDescriptionRepository jobRepository;

	@Autowired
	private RegistrationViewRepository viewRepository;

	@Autowired
	private JobAmountRepository jobAmountRepository;

	@Autowired
	private ExamCenterRepository centerRepository;

	@Autowired
	private JobDetailsRepository jobDetailsRepository;

	@Autowired
	private AuthorityRepository authorityRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ShortListAPersonalRepository shortListPersonalRepository;
	
	@Autowired 
	private ShortListAJobDetailsRepository shortListAJobDetailsRepository;
	@Autowired
	private ZoneRepository zoneRepository;
	
	@Autowired
	private MailService mailservice;
	

	public List<JobsDescription> saveJobs(List<JobsFormDescription> jobsFormDesc) {
		List<JobsDescription> jdList = new ArrayList<JobsDescription>();
		for (int i = 0; i < jobsFormDesc.size(); i++) {
			String jobname = jobsFormDesc.get(i).getJobTitle();
			String jobdesc = jobsFormDesc.get(i).getJobDesc();
			JobsDescription jobsDesc = new JobsDescription();
			jobsDesc.setJobTitle(jobname);
			jobsDesc.setJobDesc(jobdesc);
			jobsDesc.setCreatedBy(SecurityUtils.getCurrentUserLogin());
			jobsDesc.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
			jobRepository.save(jobsDesc);
			jdList.add(jobsDesc);
		}
		return jdList;
	}

	public void saveFields(JobFormFields fields) {

		RegistrationViewBean viewBean = new RegistrationViewBean();
		viewBean.setCinNumberForm(fields.isCinNumberForm());
		viewBean.setRegtinNumberForm(fields.isRegtinNumberForm());
		viewBean.setCompanyNameForm(fields.isCompanyNameForm());
		viewBean.setDegreeForm(fields.isDegreeForm());
		viewBean.setItiForm(fields.isItiForm());
		viewBean.setDiplomaForm(fields.isDiplomaForm());
		viewBean.setDrivingLicenseForm(fields.isDrivingLicenseForm());
		viewBean.setDrivingLicenseHgmvForm(fields.isDrivingLicenseHgmvForm());
		viewBean.setDrivingLicenseLmvForm(fields.isDrivingLicenseLmvForm());
		viewBean.setDrivingLicenceRoadForm(fields.isDrivingLicenceRoadForm());
		viewBean.setDrivingLicenceOegForm(fields.isDrivingLicenceOegForm());
		viewBean.setEmploymentPeriodForm(fields.isEmploymentPeriodForm());
		viewBean.setEnglishTypingForm(fields.isEnglishTypingForm());
		viewBean.setHindiTypingForm(fields.isHindiTypingForm());
		viewBean.setEnglishStenographyForm(fields.isEnglishStenographyForm());
		viewBean.setHindiStenographyForm(fields.isHindiStenographyForm());
		viewBean.setDrivingExpUploadView(fields.isDrivingExpUploadView());
		viewBean.setDrivingUploadView(fields.isDrivingUploadView());
		// viewBean.setEssentialQualificationForm(fields.isEssentialQualificationForm());
		viewBean.setExperienceCertificateForm(fields.isExperienceCertificateForm());
		viewBean.setExperienceForm(fields.isExperienceForm());
		viewBean.setGraduationForm(fields.isGraduationForm());
		// viewBean.setHighSchoolForm(fields.isHighSchoolForm());
		viewBean.setIntermediateForm(fields.isIntermediateForm());
		viewBean.setMonthlySalaryForm(fields.isMonthlySalaryForm());
		viewBean.setNccbCertificateForm(fields.isNccbCertificateForm());
		viewBean.setNccbUploadForm(fields.isNccbUploadForm());
		viewBean.setNccCertificateForm(fields.isNccCertificateForm());
		viewBean.setNccCUploadForm(fields.isNccCUploadForm());
		viewBean.setOtherQualificationForm(fields.isOtherQualificationForm());
		viewBean.setPostGraduationForm(fields.isPostGraduationForm());
		viewBean.setSportCertificateForm(fields.isSportCertificateForm());
		viewBean.setSportsUploadForm(fields.isSportsUploadForm());
		viewBean.setTechnicalForm(fields.isTechnicalForm());
		viewBean.setTemporaryPermanentForm(fields.isTemporaryPermanentForm());
		viewBean.setTypingForm(fields.isTypingForm());
		viewBean.setStenographyForm(fields.isStenographyForm());
		viewBean.setWorkNatureForm(fields.isWorkNatureForm());
		viewBean.setCreatedBy(SecurityUtils.getCurrentUserLogin());
		viewBean.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());

		Degree degree = new Degree();
		// Degree fields = fields.getDegree();
		degree.setDegreeCertificateView(fields.isDegreeCertificateView());
		degree.setDegreeCourseTypeView(fields.isDegreeCourseTypeView());
		degree.setDegreeInstitutionView(fields.isDegreeInstitutionView());
		degree.setDegreeObtainarksView(fields.isDegreeObtainarksView());
		degree.setDegreePercentageView(fields.isDegreePercentageView());
		degree.setDegreeSpecializationView(fields.isDegreeSpecializationView());
		degree.setDegreeTotalMarksView(fields.isDegreeTotalMarksView());
		degree.setDegreeUniversityView(fields.isDegreeUniversityView());
		degree.setDegreeYearView(fields.isDegreeYearView());
		degree.setDegreeMinPercent(fields.getDegreeMinPercent());
		degree.setDegreeBranchView(fields.isDegreeBranchView());
		viewBean.setDegree(degree);
		Iti Iti = new Iti();
		// Iti fields = fields.getIti();
		Iti.setItiCertificateView(fields.isItiCertificateView());
		Iti.setItiCourseTypeView(fields.isItiCourseTypeView());
		Iti.setItiInstitutionView(fields.isItiInstitutionView());
		Iti.setItiObtainarksView(fields.isItiObtainarksView());
		Iti.setItiPercentageView(fields.isItiPercentageView());
		Iti.setItiSpecializationView(fields.isItiSpecializationView());
		Iti.setItiTotalMarksView(fields.isItiTotalMarksView());
		Iti.setItiUniversityView(fields.isItiUniversityView());
		Iti.setItiYearView(fields.isItiYearView());
		Iti.setItiMinPercent(fields.getItiMinPercent());
		Iti.setItiBranchView(fields.isItiBranchView());

		viewBean.setIti(Iti);
		Diploma diploma = new Diploma();
		// Diploma fields = fields.getDiploma();
		diploma.setDiplomaCertificateView(fields.isDiplomaCertificateView());
		diploma.setDiplomaCourseTypeView(fields.isDiplomaCourseTypeView());
		diploma.setDiplomaInstitutionView(fields.isDiplomaInstitutionView());
		diploma.setDiplomaObtainarksView(fields.isDiplomaObtainarksView());
		diploma.setDiplomaPercentageView(fields.isDiplomaPercentageView());
		diploma.setDiplomaSpecializationView(fields.isDiplomaSpecializationView());
		diploma.setDiplomaTotalMarksView(fields.isDiplomaTotalMarksView());
		diploma.setDiplomaUniversityView(fields.isDiplomaUniversityView());
		diploma.setDiplomaYearView(fields.isDiplomaYearView());
		diploma.setDiplomaMinPercent(fields.getDiplomaMinPercent());
		diploma.setDiplomaBranchView(fields.isDiplomaBranchView());
		viewBean.setDiploma(diploma);

		DrivingHgmv drivingHgmv = new DrivingHgmv();
		// DrivingHgmv hgmv = fields.getDrivingHgmv();
		// drivingHgmv.setHgmvExperienceView(fields.isHgmvExperienceView());
		drivingHgmv.setIssueDateHgmvView(fields.isIssueDateHgmvView());
		drivingHgmv.setIssuingAuthorityHgmvView(fields.isIssuingAuthorityHgmvView());
		drivingHgmv.setLicenceNumberHgmvView(fields.isLicenceNumberHgmvView());
		drivingHgmv.setValidUptoHgmvView(fields.isValidUptoHgmvView());
		viewBean.setDhgmv(drivingHgmv);

		DrivingLmv drivingLmv = new DrivingLmv();
		// DrivingLmv lmv = fields.getDrivinglmv();
		// drivingLmv.setLmvExperienceView(fields.isLmvExperienceView());
		drivingLmv.setIssueDateLmvView(fields.isIssueDateLmvView());
		drivingLmv.setIssuingAuthorityLmvView(fields.isIssuingAuthorityLmvView());
		drivingLmv.setLicenceNumberLmvView(fields.isLicenceNumberLmvView());
		drivingLmv.setValidUptoLmvView(fields.isValidUptoLmvView());
		viewBean.setDlmv(drivingLmv);
		DrivingRoadRoller drivingRoadRoller = new DrivingRoadRoller();
		// DrivingHgmv hgmv = fields.getDrivingHgmv();
		// drivingHgmv.setHgmvExperienceView(fields.isHgmvExperienceView());
		drivingRoadRoller.setIssueDateRoadView(fields.isIssueDateRoadView());
		drivingRoadRoller.setIssuingAuthorityRoadView(fields.isIssuingAuthorityRoadView());
		drivingRoadRoller.setLicenceNumberRoadView(fields.isLicenceNumberRoadView());
		drivingRoadRoller.setValidUptoRoadView(fields.isValidUptoRoadView());
		viewBean.setRoad(drivingRoadRoller);

		LoaderExcavator loaderExcavator = new LoaderExcavator();
		// DrivingHgmv hgmv = fields.getDrivingHgmv();
		// drivingHgmv.setHgmvExperienceView(fields.isHgmvExperienceView());
		loaderExcavator.setIssueDateOegView(fields.isIssueDateOegView());
		loaderExcavator.setIssuingAuthorityOegView(fields.isIssuingAuthorityOegView());
		loaderExcavator.setLicenceNumberOegView(fields.isLicenceNumberOegView());
		loaderExcavator.setValidUptoOegView(fields.isValidUptoOegView());
		viewBean.setOeg(loaderExcavator);

		Graduation graduation = new Graduation();
		// Graduation fields = fields.getGraduation();
		graduation.setUgCertificateView(fields.isUgCertificateView());
		graduation.setUgCourseTypeView(fields.isUgCourseTypeView());
		graduation.setUgCourseView(fields.isUgCourseView());
		graduation.setUgInstitutionView(fields.isUgInstitutionView());
		graduation.setUgObtainarksView(fields.isUgObtainarksView());
		graduation.setUgPercentageView(fields.isUgPercentageView());
		graduation.setUgSpecializationView(fields.isUgSpecializationView());
		graduation.setUgTotalMarksView(fields.isUgTotalMarksView());
		graduation.setUgUniversityView(fields.isUgUniversityView());
		graduation.setUgYearView(fields.isUgYearView());
		graduation.setUgMinPercent(fields.getUgMinPercent());
		viewBean.setGraduation(graduation);

		PostGraduation postGraduation = new PostGraduation();
		// PostGraduation fields = fields.getPostGraduation();
		postGraduation.setPgCertificateView(fields.isPgCertificateView());
		postGraduation.setPgCourseTypeView(fields.isPgCourseTypeView());
		postGraduation.setPgCourseView(fields.isPgCourseView());
		postGraduation.setPgInstitutionView(fields.isPgInstitutionView());
		postGraduation.setPgObtainarksView(fields.isPgObtainarksView());
		postGraduation.setPgPercentageView(fields.isPgPercentageView());
		postGraduation.setPgSpecializationView(fields.isPgSpecializationView());
		postGraduation.setPgTotalMarksView(fields.isPgTotalMarksView());
		postGraduation.setPgUniversityView(fields.isPgUniversityView());
		postGraduation.setPgYearView(fields.isPgYearView());
		postGraduation.setPgMinPercent(fields.getPgMinPercent());
		viewBean.setPgraduation(postGraduation);
		

		// HighSchool highSchool = new HighSchool();
		// highSchool.setHighBoardView(fields.isHighBoardView());
		// highSchool.setHighCertificateView(fields.isHighCertificateView());
		// highSchool.setHighCourseTypeView(fields.isHighCourseTypeView());
		// highSchool.setHighInstitutionView(fields.isHighInstitutionView());
		// highSchool.setHighObtainarksView(fields.isHighObtainarksView());
		// highSchool.setHighPercentageView(fields.isHighPercentageView());
		// highSchool.setHighSpecializationView(fields.isHighSpecializationView());
		// highSchool.setHighTotalMarksView(fields.isHighTotalMarksView());
		// highSchool.setHighYearView(fields.isHighYearView());
		// highSchool.setHighMinPercent(fields.getHighMinPercent());
		// viewBean.setHigh(highSchool);

		Intermediate intermediate = new Intermediate();
		// Intermediate fields = fields.getIntermediate();
		intermediate.setInterBoardView(fields.isInterBoardView());
		intermediate.setInterCertificateView(fields.isInterCertificateView());
		intermediate.setInterCourseTypeView(fields.isInterCourseTypeView());
		intermediate.setInterInstitutionView(fields.isInterInstitutionView());
		intermediate.setInterObtainarksView(fields.isInterObtainarksView());
		intermediate.setInterPercentageView(fields.isInterPercentageView());
		intermediate.setInterSpecializationView(fields.isInterSpecializationView());
		intermediate.setInterTotalMarksView(fields.isInterTotalMarksView());
		intermediate.setInterYearView(fields.isInterYearView());
		intermediate.setInterMinPercent(fields.getInterMinPercent());
		intermediate.setInterGradeView(fields.isInterGradeView());
		intermediate.setAdditionalCertificateView(fields.isAdditionalCertificateView());
		intermediate.setFirstaidCertificateView(fields.isFirstaidCertificateView());
		viewBean.setInter(intermediate);
		

		RecruitmentZone zone = new RecruitmentZone();
		// RecruitmentZone fields = fields.getRecruitmentZone();
		zone.setPuneZone(fields.isPuneZone());
		zone.setRishikeshZone(fields.isRishikeshZone());
		zone.setTezpurZone(fields.isTezpurZone());
		viewBean.setZone(zone);

		JobDetails jobDetails = new JobDetails();
		jobDetails.setActiveDate(fields.getActiveDate());
		jobDetails.setExpiryDate(fields.getExpiryDate());
		jobDetails.setFeeSubmitDate(fields.getFeeSubmitDate());
		jobDetails.setFeeSubmitLastDate(fields.getFeeSubmitLastDate());
		jobDetails.setPostName(fields.getPostName());
		jobDetails.setPostProfile(fields.getPostProfile());
		jobDetails.setCreatedBy(SecurityUtils.getCurrentUserLogin());
		jobDetails.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		viewBean.setJobDetails(jobDetails);
		JobAmountDetails details = null;

		for (int i = 0; i < fields.getCategoryAmount().size(); i++) {
			details = new JobAmountDetails();
			details.setAmount(fields.getCategoryAmount().get(i).getAmount());
			details.setMaxAge(fields.getCategoryAmount().get(i).getMaxAge());
			details.setMinAge(fields.getCategoryAmount().get(i).getMinAge());
			details.setPostNumber(fields.getCategoryAmount().get(i).getPostNumber());
			details.setCategoryName(fields.getCategoryAmount().get(i).getCategoryName());
			details.setAgeRelaxation(fields.getCategoryAmount().get(i).getAgeRelaxation());
			details.setPercentageRelaxation(fields.getCategoryAmount().get(i).getPercentageRelaxation());
			details.setJobDetails(jobDetails);
			details.setViewBean(viewBean);
			List<JobAmountDetails> jobAmountDetails = new ArrayList<JobAmountDetails>();
			jobAmountDetails.add(details);
			viewBean.setJobAmountDetails(jobAmountDetails);

			jobAmountRepository.save(details);
		}

		viewRepository.save(viewBean);

	}

	public void uploadJobDocument(MultipartFile postDetailsDocument, long jobid) {

		JobDetails detail = new JobDetails();

		try {
			String JOBDOCUMENT = uploadDocuments(jobid, postDetailsDocument, "JOBDOCUMENT");

			detail.setPostDetailsDocument(JOBDOCUMENT != null ? JOBDOCUMENT : null);

			jobDetailsRepository.update(JOBDOCUMENT, jobid);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String uploadDocuments(long jobId, MultipartFile multipartFile, String fileNamePrefix) {

		String newDocName = "";
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(UPLOADED_FOLDER + jobId + "/");
				if (!doc.exists()) {
					log.info(" Dir " + jobId + " NOT Exists");
					doc.mkdirs();
				}
				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("trim file name" + fileName);
				}
				String folder_name = UPLOADED_FOLDER + jobId + "/" + fileName;
				log.info("orignal folder_name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + fileName;
					System.out.println("New Document nmae " + newDocName);
				} else {
					newDocName = fileName;
					System.out.println("New Document nmae " + newDocName);

				}
				String newPath = UPLOADED_FOLDER + jobId + "/" + newDocName;
				log.info("rename file : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				System.out.println("path" + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			log.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	public void adminRegister(AdminRegister register) {
		User user = new User();
		Zone zone = new Zone();
		user.setLogin(register.getEmail());
		user.setFirstName(register.getFirstName());
		user.setLastName(register.getLastName());
		user.setEmail(register.getEmail());
		user.setActivated(true);
		if (register.getLangKey() == null) {
			user.setLangKey("en"); // default language
		}
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		if (register.getAuthorities() != null) {
			Set<Authority> authorities = new HashSet<>();
			register.getAuthorities().forEach(authority -> authorities.add(authorityRepository.findOne(authority)));
			user.setAuthorities(authorities);
		}

		String encryptedPassword = passwordEncoder.encode(register.getPassword());
		user.setPassword(encryptedPassword);
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		user.setActivated(true);
		user.setCreatedBy(register.getEmail());
		user.setLastModifiedBy(register.getEmail());
		zone.setName(register.getZone());
		zone.setUser(user);
		zoneRepository.save(zone);
		log.debug("Created Information for User: {}", user);
	}

	public void saveCenter(ExaminationCenter exam) {
		CenterAddress address = new CenterAddress();
		address.setCenterAddress(exam.getCenterAddress());
		ExaminationCenter center = new ExaminationCenter();
		center.setStateName(exam.getStateName());
		center.setCityName(exam.getCityName());
		center.setAvailableSeats(exam.getAvailableSeats());
		center.setCenterAddress(exam.getCenterAddress());
		center.setCenterName(exam.getCenterName());
		center.setCreatedBy(SecurityUtils.getCurrentUserLogin());
		center.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		center.setAddress(address);
		centerRepository.save(center);
	}

	public void saveShortListCandidate(List<ShortListAPersonalDetails> personal) {

		for (int i = 0; i < personal.size(); i++) {
			
			ShortListAEssentialQualification equalification =  personal.get(i).getEssential();
			ShortListADrivingLicense sdriving =  personal.get(i).getDriving();
			ShortListAOrganisationExperience oexperience = personal.get(i).getExperience();
			ShortListATechnicalQualification tqualification = personal.get(i).getTechnical();
			ShortListAGraduationQualification gqualification = personal.get(i).getGraduation();
			ShortListAUploadCertificate ucertificate = personal.get(i).getCertificate();
			ShortListAJobDetails sjobdetails= personal.get(i).getJobDetails();
			
			ShortListAPersonalDetails personalDetails = new ShortListAPersonalDetails();
			personalDetails.setRegistrationId(personal.get(i).getRegistrationId());
			personalDetails.setPayment(personal.get(i).isPayment());
			personalDetails.setFirstName(personal.get(i).getFirstName());
			personalDetails.setMiddleName(personal.get(i).getMiddleName());
			personalDetails.setLastName(personal.get(i).getLastName());
			personalDetails.setFatherName(personal.get(i).getFatherName());
			personalDetails.setMotherName(personal.get(i).getMotherName());
			personalDetails.setDateOfBirth(personal.get(i).getDateOfBirth());
			personalDetails.setMobileNumber(personal.get(i).getMobileNumber());
			personalDetails.setEmailAddress(personal.get(i).getEmailAddress());
			personalDetails.setNationality(personal.get(i).getNationality());
			personalDetails.setAadhaarCard(personal.get(i).isAadhaarCard());
			personalDetails.setAadhaarNumber(personal.get(i).getAadhaarNumber());
			personalDetails.setMaritalStatus(personal.get(i).getMaritalStatus());
			personalDetails.setCategory(personal.get(i).getCategory());
			personalDetails.setBelongCommunity(personal.get(i).isBelongCommunity());
			personalDetails.setCommunityName(personal.get(i).getCommunityName());
			personalDetails.setPermanentAddress(personal.get(i).getPermanentAddress());
			personalDetails.setPermanentCity(personal.get(i).getPermanentCity());
			personalDetails.setPermanentState(personal.get(i).getPermanentState());
			personalDetails.setPermanentPincode(personal.get(i).getPermanentPincode());
			personalDetails.setPostalAddress(personal.get(i).getPostalAddress());
			personalDetails.setPostalCity(personal.get(i).getPostalCity());
			personalDetails.setPostalState(personal.get(i).getPostalState());
			personalDetails.setPostalPincode(personal.get(i).getPostalPincode());
			personalDetails.setSonDaughterExservice(personal.get(i).isSonDaughterGref());
			personalDetails.setArmyRank(personal.get(i).getArmyRank());
			personalDetails.setArmyNumber(personal.get(i).getArmyNumber());
			personalDetails.setSonDaughterGref(personal.get(i).isSonDaughterGref());
			personalDetails.setSonDaughterNumber(personal.get(i).getSonDaughterNumber());
			personalDetails.setSonDaughterRank(personal.get(i).getSonDaughterRank());
			personalDetails.setSonDaughterName(personal.get(i).getSonDaughterName());
			personalDetails.setBrotherSisterGref(personal.get(i).isBrotherSisterGref());
			personalDetails.setBrotherSisterNumber(personal.get(i).getBrotherSisterNumber());
			personalDetails.setBrotherSisterRank(personal.get(i).getBrotherSisterRank());
			personalDetails.setBrotherSisterName(personal.get(i).getBrotherSisterName());
			personalDetails.setNccB(personal.get(i).isNccB());
			personalDetails.setCandidateHeight((personal.get(i).getCandidateHeight()));
			personalDetails.setCandidateWeight(personal.get(i).getCandidateWeight());
			personalDetails.setBloodGroup(personal.get(i).getBloodGroup());
			personalDetails.setEyeSight(personal.get(i).getEyeSight());
			personalDetails.setAge(personal.get(i).getAge());
			personalDetails.setDisablity(personal.get(i).isDisablity());
			personalDetails.setDisabilitySpecify((personal.get(i).getDisabilitySpecify()));
			personalDetails.setCentralGovt(personal.get(i).isCentralGovt());

			personalDetails.setArmedForce(personal.get(i).isArmedForce());

			personalDetails.setServedFrom(personal.get(i).getServedFrom());
			personalDetails.setServedTo(personal.get(i).getServedTo());
			personalDetails.setArmedarmyNumber(personal.get(i).getArmedarmyNumber());

			personalDetails.setArmedFrom(personal.get(i).getArmedFrom());
			personalDetails.setArmedTo(personal.get(i).getArmedTo());

			personalDetails.setCentralGovtCertificate(
					personal.get(i).getCentralGovtCertificate() != null ? personal.get(i).getCentralGovtCertificate()
							: null);

			personalDetails.setArmeddisCertificate(
					personal.get(i).getArmeddisCertificate() != null ? personal.get(i).getArmeddisCertificate() : null);

			personalDetails.setNccbCertificate(
					personal.get(i).getNccbCertificate() != null ? personal.get(i).getNccbCertificate() : null);
			personalDetails.setAadharCertificate(
					personal.get(i).getAadharCertificate() != null ? personal.get(i).getAadharCertificate() : null);
             
			personalDetails.setCategoryCertificate(
					personal.get(i).getCategoryCertificate() != null ? personal.get(i).getCategoryCertificate() : null);
			
			
			
			personalDetails.setNccC(personal.get(i).isNccC());

			personalDetails.setNccCertificate(
					personal.get(i).getNccCertificate() != null ? personal.get(i).getNccCertificate() : null);

			personalDetails.setSportsman(personal.get(i).isSportsman());

			personalDetails.setSportCertificate(
					personal.get(i).getSportCertificate() != null ? personal.get(i).getSportCertificate() : null);

			personalDetails.setRecruitmentZone(personal.get(i).getRecruitmentZone());
			personalDetails.setGender(personal.get(i).getGender());
			personalDetails.setFinalSubmit(personal.get(i).isFinalSubmit());
			personalDetails.setPayment(personal.get(i).isPayment());

			ShortListAJobDetails jobDetails = new ShortListAJobDetails();
			jobDetails.setPostId(sjobdetails.getPostId());
			
			
			
			personalDetails.setJobDetails(jobDetails);
			personalDetails.setJobDetails(sjobdetails);
			
			ShortListAEssentialQualification essentials = new ShortListAEssentialQualification();
			essentials.setHighBoard(equalification.getHighBoard());
			essentials.setHighCourseType(equalification.getHighCourseType());
			essentials.setHighInstitution(equalification.getHighInstitution());
			essentials.setHighObtainMarks(equalification.getHighObtainMarks());
			essentials.setHighTotalMarks(equalification.getHighTotalMarks());
			essentials.setHighPercentage(equalification.getHighPercentage());
			essentials.setHighgrade(equalification.getHighgrade());
			try {
				essentials.setHighCertificate(equalification.getHighCertificate() != null ? equalification.getHighCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
				
			}
			essentials.setHighSpecialization(equalification.getHighSpecialization());
			essentials.setHighYear(equalification.getHighYear());
			essentials.setInterBoard(equalification.getInterBoard());
			essentials.setInterCourseType(equalification.getInterCourseType());
			essentials.setInterInstitution(equalification.getInterInstitution());
			essentials.setInterObtainMarks(equalification.getInterObtainMarks());

			essentials.setInterTotalMarks(equalification.getInterTotalMarks());
			essentials.setInterPercentage(equalification.getInterPercentage());
			essentials.setIntergrade(equalification.getIntergrade());
			try {
				essentials.setInterCertificate(equalification.getInterCertificate() != null ? equalification.getInterCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			essentials.setInterSpecialization(equalification.getInterSpecialization());
			essentials.setInterYear(equalification.getInterYear());
			essentials.setHindiTyping(equalification.getHindiTyping());
			essentials.setEnglishTyping(equalification.getEnglishTyping());
			essentials.setHindiStenography(equalification.getHindiStenography());
			essentials.setEnglishStenography(equalification.getEnglishStenography());
			try {
				essentials.setAdditionalCertificate(equalification.getAdditionalCertificate() != null ? equalification.getAdditionalCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}

			try {
				essentials.setFirstaidCertificate(equalification.getFirstaidCertificate() != null ? equalification.getFirstaidCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// essentialRepository.save(essentials);

			personalDetails.setEssential(essentials);
			// personal.get(i).setEssential(essentials);

			ShortListADrivingLicense driving = new ShortListADrivingLicense();
			driving.setIssueDateLmv(sdriving.getIssueDateLmv());
			driving.setIssuingAuthorityLmv(sdriving.getIssuingAuthorityLmv());
			driving.setLicenceNumberLmv(sdriving.getLicenceNumberLmv());
			driving.setValidUptoLmv(sdriving.getValidUptoLmv());
			driving.setIssueDateHgmv(sdriving.getIssueDateHgmv());
			driving.setIssuingAuthorityHlmv(sdriving.getIssuingAuthorityHlmv());
			driving.setLicenceNumberHgmv(sdriving.getLicenceNumberHgmv());
			driving.setValidUptoHgmv(sdriving.getValidUptoHgmv());
			driving.setIssueDateRoad(sdriving.getIssueDateRoad());
			driving.setIssuingAuthorityRoad(sdriving.getIssuingAuthorityRoad());
			driving.setLicenceNumberRoad(sdriving.getLicenceNumberRoad());
			driving.setValidUptoRoad(sdriving.getValidUptoRoad());
			driving.setIssueDateOeg(sdriving.getIssueDateOeg());
			driving.setIssuingAuthorityOeg(sdriving.getIssuingAuthorityOeg());
			driving.setLicenceNumberOeg(sdriving.getLicenceNumberOeg());
			driving.setValidUptoOeg(sdriving.getValidUptoOeg());

			try {
				driving.setUploadLicence(sdriving.getUploadLicence() != null ? sdriving.getUploadLicence() : null);

				driving.setDrivingExperience(sdriving.getDrivingExperience() != null ? sdriving.getDrivingExperience() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// sdriving.setLicence(driving);
			personalDetails.setDriving(driving);
			ShortListAOrganisationExperience experience = new ShortListAOrganisationExperience();
			experience.setCompanyName(oexperience.getCompanyName());
			experience.setEmploymentFrom(oexperience.getEmploymentFrom());
			experience.setEmploymentTo(oexperience.getEmploymentTo());
			experience.setCinNumber(oexperience.getCinNumber());
			experience.setRegtinNumber(oexperience.getRegtinNumber());
			experience.setTemporaryPermanent(oexperience.getTemporaryPermanent());
			experience.setWorkNature(oexperience.getWorkNature());
			experience.setMonthlySalary(oexperience.getMonthlySalary());
			try {
				experience.setExperienceCertificate(experience.getExperienceCertificate() != null ? experience.getExperienceCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// oexperience.setExperience(experience);

			personalDetails.setExperience(experience);

			ShortListATechnicalQualification technicals = new ShortListATechnicalQualification();
			technicals.setDegreeYear(tqualification.getDegreeYear());
			technicals.setDegreeBranch(tqualification.getDegreeBranch());
			technicals.setDegreeCourseType(tqualification.getDegreeCourseType());
			technicals.setDegreeInstitution(tqualification.getDegreeInstitution());
			technicals.setDegreeMarksObtain(tqualification.getDegreeMarksObtain());
			technicals.setDegreeTotalMarks(tqualification.getDiplomaTotalMarks());
			technicals.setDegreePercentage(tqualification.getDegreePercentage());
			technicals.setDegreeSpecialization(tqualification.getDegreeSpecialization());
			technicals.setDegreeUniversity(tqualification.getDegreeUniversity());
			try {
				technicals.setDegreeCertificate(technicals.getDegreeCertificate() != null ? technicals.getDegreeCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			technicals.setDiplomaBranch(tqualification.getDiplomaBranch());
			technicals.setDiplomaYear(tqualification.getDiplomaYear());
			technicals.setDiplomaCourseType(tqualification.getDiplomaCourseType());
			technicals.setDiplomaInstitution(tqualification.getDiplomaInstitution());
			technicals.setDiplomaObtainMarks(tqualification.getDiplomaObtainMarks());
			technicals.setDiplomaTotalMarks(tqualification.getDiplomaTotalMarks());
			technicals.setDiplomaPercentage(tqualification.getDiplomaPercentage());
			technicals.setDiplomaUniversity(tqualification.getDiplomaUniversity());
			technicals.setDiplomaSpecialization(tqualification.getDiplomaSpecialization());
			try {
				technicals.setDiplomaCertificate(technicals.getDiplomaCertificate() != null ? technicals.getDiplomaCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// tqualification.setTechnical(technicals);

			technicals.setItiYear(tqualification.getItiYear());
			technicals.setItiBranch(tqualification.getItiBranch());
			technicals.setItiCourseType(tqualification.getItiCourseType());
			technicals.setItiInstitution(tqualification.getItiInstitution());
			technicals.setItiMarksObtain(tqualification.getItiMarksObtain());
			technicals.setItiTotalMarks(tqualification.getDiplomaTotalMarks());
			technicals.setItiPercentage(tqualification.getItiPercentage());
			technicals.setItiSpecialization(tqualification.getItiSpecialization());
			technicals.setItiUniversity(tqualification.getItiUniversity());
			try {
				technicals.setItiCertificate(technicals.getItiCertificate() != null ? technicals.getItiCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}

			personalDetails.setTechnical(technicals);

			ShortListAGraduationQualification qualify = new ShortListAGraduationQualification();
			qualify.setPgInstitution(gqualification.getPgInstitution());
			qualify.setPgCourse(gqualification.getPgCourse());
			qualify.setPgCourseType(gqualification.getPgCourseType());
			qualify.setPgObtainMarks(gqualification.getPgObtainMarks());
			qualify.setPgTotalMarks(gqualification.getPgTotalMarks());
			qualify.setPgPassingYear(gqualification.getPgPassingYear());
			qualify.setPgPercentage(gqualification.getPgPercentage());
			qualify.setPgSpecialization(gqualification.getPgSpecialization());
			qualify.setPgUniversity(gqualification.getPgUniversity());
			try {
				qualify.setPgCertificate(qualify.getPgCertificate() != null ? qualify.getPgCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			qualify.setUgInstitution(gqualification.getUgInstitution());
			qualify.setUgCourse(gqualification.getUgCourse());
			qualify.setUgCourseType(gqualification.getUgCourseType());
			qualify.setUgObtainMarks(gqualification.getUgObtainMarks());
			qualify.setUgTotalMarks(gqualification.getUgTotalMarks());
			qualify.setUgPassingYear(gqualification.getUgPassingYear());
			qualify.setUgPercentage(gqualification.getUgPercentage());
			qualify.setUgSpecialization(gqualification.getUgSpecialization());
			qualify.setUgUniversity(gqualification.getUgUniversity());
			try {
				qualify.setUgCertificate(qualify.getUgCertificate() != null ? qualify.getUgCertificate() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// gqualification.setGraduation(qualify);
			

			personalDetails.setGraduation(qualify);

			ShortListAUploadCertificate certificate = new ShortListAUploadCertificate();
			try {
				certificate.setBrowsePhoto(ucertificate.getBrowsePhoto() != null ? ucertificate.getBrowsePhoto() : null);
			} catch (Exception e) {
			}
			try {
				certificate.setBrowseSignature(ucertificate.getBrowseSignature() != null ? ucertificate.getBrowseSignature() : null);
			} catch (Exception e) {
				e.getMessage();
			}
			// register.setUpload(certificate);
			personalDetails.setCertificate(certificate);

			shortListPersonalRepository.save(personalDetails);
			shortListAJobDetailsRepository.save(sjobdetails);
			
		}
	}
	
	
	

}
