ALTER TABLE `bro_orb`.`bro_candidate_essential_qualification` 
CHANGE COLUMN `hss_board` `hss_board` VARCHAR(255) NOT NULL ,
CHANGE COLUMN `hss_course_type` `hss_course_type` VARCHAR(50) NOT NULL ,
CHANGE COLUMN `hss_institution` `hss_institution` VARCHAR(255) NOT NULL ,
CHANGE COLUMN `sss_board` `sss_board` VARCHAR(255) NOT NULL ,
CHANGE COLUMN `sss_course_type` `sss_course_type` VARCHAR(50) NOT NULL ,
CHANGE COLUMN `sss_institution` `sss_institution` VARCHAR(255) NOT NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_essential_qualification` 
CHANGE COLUMN `hss_certificate` `hss_certificate` VARCHAR(20) NULL ,
CHANGE COLUMN `sss_certificate` `sss_certificate` VARCHAR(20) NULL ;

ALTER TABLE `bro_orb`.`bro_certificate_upload` 
CHANGE COLUMN `browse_photo` `browse_photo` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `browse_signature` `browse_signature` VARCHAR(100) NULL DEFAULT NULL ;




ALTER TABLE `bro_orb`.`bro_candidate_driving_license` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `issue_date_hgmv` `issue_date_hgmv` DATETIME NULL ,
CHANGE COLUMN `issue_date_lmv` `issue_date_lmv` DATETIME NULL ,
CHANGE COLUMN `issuing_authority_hlmv` `issuing_authority_hlmv` VARCHAR(50) NULL ,
CHANGE COLUMN `issuing_authority_lmv` `issuing_authority_lmv` VARCHAR(50) NULL ,
CHANGE COLUMN `licence_number_hgmv` `licence_number_hgmv` VARCHAR(50) NULL ,
CHANGE COLUMN `licence_number_lmv` `licence_number_lmv` VARCHAR(50) NULL ,
CHANGE COLUMN `valid_upto_hgmv` `valid_upto_hgmv` DATETIME NULL ,
CHANGE COLUMN `valid_upto_lmv` `valid_upto_lmv` DATETIME NULL ;

ALTER TABLE `bro_orb`.`bro_candidate_experience` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `organization_name` `organization_name` VARCHAR(50) NULL ,
CHANGE COLUMN `cin_number` `cin_number` VARCHAR(20) NULL ,
CHANGE COLUMN `employment_period_from` `employment_period_from` DATETIME NULL ,
CHANGE COLUMN `employment_period_to` `employment_period_to` DATETIME NULL ,
CHANGE COLUMN `monthly_salary` `monthly_salary` BIGINT(20) NULL ,
CHANGE COLUMN `temporary_permanent` `temporary_permanent` VARCHAR(20) NULL ,
CHANGE COLUMN `work_nature` `work_nature` VARCHAR(50) NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_technical_qualification` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `degree_branch` `degree_branch` VARCHAR(20) NULL ,
CHANGE COLUMN `degree_certificate` `degree_certificate` VARCHAR(10) NULL ,
CHANGE COLUMN `degree_course_type` `degree_course_type` VARCHAR(20) NULL ,
CHANGE COLUMN `degree_institution` `degree_institution` VARCHAR(50) NULL ,
CHANGE COLUMN `degree_marks_obtain` `degree_marks_obtain` INT(11) NULL ,
CHANGE COLUMN `degree_percentage` `degree_percentage` FLOAT NULL ,
CHANGE COLUMN `degree_specialization` `degree_specialization` VARCHAR(20) NULL ,
CHANGE COLUMN `degree_total_marks` `degree_total_marks` INT(11) NULL ,
CHANGE COLUMN `degree_university` `degree_university` VARCHAR(10) NULL ,
CHANGE COLUMN `degree_passing_year` `degree_passing_year` INT(11) NULL ,
CHANGE COLUMN `diploma_branch` `diploma_branch` VARCHAR(50) NULL ,
CHANGE COLUMN `diploma_certificate` `diploma_certificate` VARCHAR(20) NULL ,
CHANGE COLUMN `diploma_course_type` `diploma_course_type` VARCHAR(20) NULL ,
CHANGE COLUMN `diploma_institution` `diploma_institution` VARCHAR(50) NULL ,
CHANGE COLUMN `diploma_marks_obtain` `diploma_marks_obtain` INT(11) NULL ,
CHANGE COLUMN `diploma_percentage` `diploma_percentage` FLOAT NULL ,
CHANGE COLUMN `diploma_specialization` `diploma_specialization` VARCHAR(20) NULL ,
CHANGE COLUMN `diploma_total_marks` `diploma_total_marks` INT(11) NULL ,
CHANGE COLUMN `diploma_university` `diploma_university` VARCHAR(50) NULL ,
CHANGE COLUMN `diploma_passing_year` `diploma_passing_year` INT(11) NULL ;

ALTER TABLE `bro_orb`.`bro_candidate_ug_pg_details` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `pg_course` `pg_course` VARCHAR(50) NULL ,
CHANGE COLUMN `pg_course_type` `pg_course_type` VARCHAR(50) NULL ,
CHANGE COLUMN `pg_institution` `pg_institution` VARCHAR(100) NULL ,
CHANGE COLUMN `pg_obtain_marks` `pg_obtain_marks` INT(11) NULL ,
CHANGE COLUMN `pg_passing_year` `pg_passing_year` INT(11) NULL ,
CHANGE COLUMN `pg_percentage` `pg_percentage` FLOAT NULL ,
CHANGE COLUMN `pg_specialization` `pg_specialization` VARCHAR(50) NULL ,
CHANGE COLUMN `pg_total_marks` `pg_total_marks` INT(11) NULL ,
CHANGE COLUMN `pg_university` `pg_university` VARCHAR(100) NULL ,
CHANGE COLUMN `ug_course` `ug_course` VARCHAR(50) NULL ,
CHANGE COLUMN `ug_course_type` `ug_course_type` VARCHAR(50) NULL ,
CHANGE COLUMN `ug_institution` `ug_institution` VARCHAR(100) NULL ,
CHANGE COLUMN `ug_obtain_marks` `ug_obtain_marks` INT(11) NULL ,
CHANGE COLUMN `ug_passing_year` `ug_passing_year` INT(11) NULL ,
CHANGE COLUMN `ug_percentage` `ug_percentage` FLOAT NULL ,
CHANGE COLUMN `ug_specialization` `ug_specialization` VARCHAR(50) NULL ,
CHANGE COLUMN `ug_total_marks` `ug_total_marks` INT(11) NULL ,
CHANGE COLUMN `ug_university` `ug_university` VARCHAR(100) NULL ;

ALTER TABLE `bro_orb`.`bro_certificate_upload` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `browse_photo` `browse_photo` VARCHAR(10) NULL ,
CHANGE COLUMN `browse_signature` `browse_signature` VARCHAR(10) NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_personal_details` 
CHANGE COLUMN `aadhaar_number` `aadhaar_number` VARCHAR(12) NULL ,
CHANGE COLUMN `community_name` `community_name` VARCHAR(50) NULL ;

ALTER TABLE `bro_orb`.`candidate_driving_license` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_essential_qualification` 
CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL ,
CHANGE COLUMN `created_date` `created_date` DATETIME NULL ,
CHANGE COLUMN `english_typing_speed` `english_typing_speed` INT(11) NULL ,
CHANGE COLUMN `hss_board` `hss_board` VARCHAR(255) NULL ,
CHANGE COLUMN `hss_course_type` `hss_course_type` VARCHAR(50) NULL ,
CHANGE COLUMN `hss_institution` `hss_institution` VARCHAR(255) NULL ,
CHANGE COLUMN `hss_marks_obtain` `hss_marks_obtain` INT(11) NULL ,
CHANGE COLUMN `hss_percentage` `hss_percentage` FLOAT NULL ,
CHANGE COLUMN `hss_specialization` `hss_specialization` VARCHAR(20) NULL ,
CHANGE COLUMN `hss_total_marks` `hss_total_marks` INT(11) NULL ,
CHANGE COLUMN `hss_passing_year` `hss_passing_year` INT(11) NULL ,
CHANGE COLUMN `hindi_typing_speed` `hindi_typing_speed` INT(11) NULL ,
CHANGE COLUMN `sss_board` `sss_board` VARCHAR(255) NULL ,
CHANGE COLUMN `sss_course_type` `sss_course_type` VARCHAR(50) NULL ,
CHANGE COLUMN `sss_institution` `sss_institution` VARCHAR(255) NULL ,
CHANGE COLUMN `sss_marks_obtain` `sss_marks_obtain` INT(11) NULL ,
CHANGE COLUMN `sss_percentage` `sss_percentage` FLOAT NULL ,
CHANGE COLUMN `sss_specialization` `sss_specialization` VARCHAR(20) NULL ,
CHANGE COLUMN `sss_total_marks` `sss_total_marks` INT(11) NULL ,
CHANGE COLUMN `sss_passing_year` `sss_passing_year` INT(11) NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_personal_details` 
CHANGE COLUMN `nccc_certificate` `nccc_certificate` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `nccb_certificate` `nccb_certificate` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `sport_certificate` `sport_certificate` VARCHAR(100) NULL DEFAULT NULL ;


ALTER TABLE `bro_orb`.`bro_candidate_technical_qualification` 
CHANGE COLUMN `degree_certificate` `degree_certificate` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `diploma_certificate` `diploma_certificate` VARCHAR(100) NULL DEFAULT NULL ;
